-- DROP SCHEMA boinae;

CREATE SCHEMA boinae AUTHORIZATION kavan;
-- boinae.eleve definition

-- Drop table

-- DROP TABLE boinae.eleve;

CREATE TABLE boinae.eleve (
	id int4 NOT NULL,
	nom varchar NULL,
	prenom varchar NULL,
	visible bool NULL,
	CONSTRAINT eleve_pk PRIMARY KEY (id)
);


-- boinae.professeur definition

-- Drop table

-- DROP TABLE boinae.professeur;

CREATE TABLE boinae.professeur (
	id int4 NOT NULL,
	nom varchar NULL,
	prenom varchar NULL,
	visible bool NULL,
	CONSTRAINT professeur_pk PRIMARY KEY (id)
);


-- boinae.instrument definition

-- Drop table

-- DROP TABLE boinae.instrument;

CREATE TABLE boinae.instrument (
	id int4 NOT NULL,
	nom varchar NULL,
	visible bool NULL,
	CONSTRAINT instrument_pk PRIMARY KEY (id)
);


-- boinae.cyclemono definition

-- Drop table

-- DROP TABLE boinae.cyclemono;

CREATE TABLE boinae.cyclemono (
	id int4 NOT NULL,
	numerocycle varchar NULL DEFAULT 1,
	anneecycle varchar NULL,
	CONSTRAINT cyclemono_pk PRIMARY KEY (id)
);


-- boinae.cyclecollectif definition

-- Drop table

-- DROP TABLE boinae.cyclecollectif;

CREATE TABLE boinae.cyclecollectif (
	id int4 NOT NULL,
	numerocycle varchar NULL,
	anneecycle varchar NULL,
	CONSTRAINT cyclecollectif_pk PRIMARY KEY (id)
);


-- boinae.collectif definition

-- Drop table

-- DROP TABLE boinae.collectif;

CREATE TABLE boinae.collectif (
	id int4 NOT NULL,
	nom varchar NULL,
	visible bool NULL,
	CONSTRAINT collectif_pk PRIMARY KEY (id)
);


-- boinae.atelier definition

-- Drop table

-- DROP TABLE boinae.atelier;

CREATE TABLE boinae.atelier (
	id int4 NOT NULL,
	nom varchar NULL,
	visible bool NULL,
	CONSTRAINT atelier_pk PRIMARY KEY (id)
);


-- boinae.projet definition

-- Drop table

-- DROP TABLE boinae.projet;

CREATE TABLE boinae.projet (
	id int4 NOT NULL,
	nom varchar NULL,
	datedebut varchar NULL,
	datefin varchar NULL,
	presentation text NULL,
	bilan text NULL,
	visible bool NULL,
	CONSTRAINT projet_pk PRIMARY KEY (id)
);


-- boinae.competence definition

-- Drop table

-- DROP TABLE boinae.competence;

CREATE TABLE boinae.competence (
	id int4 NOT NULL,
	nom varchar NULL,
	note text NULL,
	visible bool NULL,
	CONSTRAINT competence_pk PRIMARY KEY (id)
);


-- boinae.domaine definition

-- Drop table

-- DROP TABLE boinae.domaine;

CREATE TABLE boinae.domaine (
	id int4 NOT NULL,
	nom varchar NULL,
	visible bool NULL,
	CONSTRAINT domaine_pk PRIMARY KEY (id)
);


-- boinae.anneescolaire definition

-- Drop table

-- DROP TABLE boinae.anneescolaire;

CREATE TABLE boinae.anneescolaire (
	id int4 NOT NULL,
	anneescolaire bpchar(9) NULL DEFAULT ((2023 - 2024)),
	visible bool NULL,
	courante bool NULL,
	CONSTRAINT anneescolaire_pk PRIMARY KEY (id)
);


-- boinae.bilanelevemono definition

-- Drop table

-- DROP TABLE boinae.bilanelevemono;

CREATE TABLE boinae.bilanelevemono (
	id int4 NOT NULL,
	"date" varchar NULL,
	bilan text NULL,
	CONSTRAINT bilanelevemono_pk PRIMARY KEY (id)
);


-- boinae.bilanelevecollectif definition

-- Drop table

-- DROP TABLE boinae.bilanelevecollectif;

CREATE TABLE boinae.bilanelevecollectif (
	id int4 NOT NULL,
	"date" varchar NULL,
	bilan text NULL,
	CONSTRAINT bilanelevecollectif_pk PRIMARY KEY (id)
);


-- boinae.eleve_join_professeur definition

-- Drop table

-- DROP TABLE boinae.eleve_join_professeur;

CREATE TABLE boinae.eleve_join_professeur (
	ideleve int4 NULL,
	idprofesseur int4 NULL,
	CONSTRAINT eleve_join_professeur_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_professeur_fk_1 FOREIGN KEY (idprofesseur) REFERENCES boinae.professeur(id)
);


-- boinae.professeur_join_instrument definition

-- Drop table

-- DROP TABLE boinae.professeur_join_instrument;

CREATE TABLE boinae.professeur_join_instrument (
	idprofesseur int4 NULL,
	idinstrument int4 NULL,
	CONSTRAINT professeur_join_instrument_fk FOREIGN KEY (idprofesseur) REFERENCES boinae.professeur(id),
	CONSTRAINT professeur_join_instrument_fk_1 FOREIGN KEY (idinstrument) REFERENCES boinae.instrument(id)
);


-- boinae.eleve_join_cyclemono definition

-- Drop table

-- DROP TABLE boinae.eleve_join_cyclemono;

CREATE TABLE boinae.eleve_join_cyclemono (
	ideleve int4 NULL,
	idcyclemono int4 NULL,
	CONSTRAINT eleve_join_cyclemono_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_cyclemono_fk_1 FOREIGN KEY (idcyclemono) REFERENCES boinae.cyclemono(id)
);


-- boinae.eleve_join_cyclecollectif definition

-- Drop table

-- DROP TABLE boinae.eleve_join_cyclecollectif;

CREATE TABLE boinae.eleve_join_cyclecollectif (
	ideleve int4 NULL,
	idcyclecollectif int4 NULL,
	CONSTRAINT eleve_join_cyclecollectif_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_cyclecollectif_fk_1 FOREIGN KEY (idcyclecollectif) REFERENCES boinae.cyclecollectif(id)
);


-- boinae.eleve_join_atelier definition

-- Drop table

-- DROP TABLE boinae.eleve_join_atelier;

CREATE TABLE boinae.eleve_join_atelier (
	ideleve int4 NULL,
	idatelier int4 NULL,
	CONSTRAINT eleve_join_atelier_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_atelier_fk_1 FOREIGN KEY (idatelier) REFERENCES boinae.atelier(id)
);


-- boinae.eleve_join_collectif definition

-- Drop table

-- DROP TABLE boinae.eleve_join_collectif;

CREATE TABLE boinae.eleve_join_collectif (
	ideleve int4 NULL,
	idcollectif int4 NULL,
	CONSTRAINT eleve_join_collectif_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_collectif_fk_1 FOREIGN KEY (idcollectif) REFERENCES boinae.collectif(id)
);


-- boinae.collectif_join_projet definition

-- Drop table

-- DROP TABLE boinae.collectif_join_projet;

CREATE TABLE boinae.collectif_join_projet (
	idcollectif int4 NULL,
	idprojet int4 NULL,
	CONSTRAINT collectif_join_projet_fk FOREIGN KEY (idcollectif) REFERENCES boinae.collectif(id),
	CONSTRAINT collectif_join_projet_fk_1 FOREIGN KEY (idprojet) REFERENCES boinae.projet(id)
);


-- boinae.projet_join_competence definition

-- Drop table

-- DROP TABLE boinae.projet_join_competence;

CREATE TABLE boinae.projet_join_competence (
	idprojet int4 NULL,
	idcompetence int4 NULL,
	CONSTRAINT projet_join_competence_fk FOREIGN KEY (idprojet) REFERENCES boinae.projet(id),
	CONSTRAINT projet_join_competence_fk_1 FOREIGN KEY (idcompetence) REFERENCES boinae.competence(id)
);


-- boinae.competence_join_domaine definition

-- Drop table

-- DROP TABLE boinae.competence_join_domaine;

CREATE TABLE boinae.competence_join_domaine (
	idcompetence int4 NULL,
	iddomaine int4 NULL,
	CONSTRAINT competence_join_domaine_fk FOREIGN KEY (idcompetence) REFERENCES boinae.competence(id),
	CONSTRAINT competence_join_domaine_fk_1 FOREIGN KEY (iddomaine) REFERENCES boinae.domaine(id)
);


-- boinae.eleve_join_bilanelevemono definition

-- Drop table

-- DROP TABLE boinae.eleve_join_bilanelevemono;

CREATE TABLE boinae.eleve_join_bilanelevemono (
	ideleve int4 NULL,
	idprofesseur int4 NULL,
	idbilanelevemono int4 NULL,
	idanneescolaire int4 NULL,
	CONSTRAINT eleve_join_bilanelevemono_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_bilanelevemono_fk_1 FOREIGN KEY (idprofesseur) REFERENCES boinae.professeur(id),
	CONSTRAINT eleve_join_bilanelevemono_fk_2 FOREIGN KEY (idbilanelevemono) REFERENCES boinae.bilanelevemono(id),
	CONSTRAINT eleve_join_bilanelevemono_fk_3 FOREIGN KEY (idanneescolaire) REFERENCES boinae.anneescolaire(id)
);


-- boinae.eleve_join_bilanelevecollectif definition

-- Drop table

-- DROP TABLE boinae.eleve_join_bilanelevecollectif;

CREATE TABLE boinae.eleve_join_bilanelevecollectif (
	ideleve int4 NULL,
	idcollectif int4 NULL,
	idbilanelevecollectif int4 NULL,
	idanneescolaire int4 NULL,
	CONSTRAINT eleve_join_bilanelevecollectif_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_bilanelevecollectif_fk_1 FOREIGN KEY (idcollectif) REFERENCES boinae.collectif(id),
	CONSTRAINT eleve_join_bilanelevecollectif_fk_2 FOREIGN KEY (idbilanelevecollectif) REFERENCES boinae.bilanelevecollectif(id),
	CONSTRAINT eleve_join_bilanelevecollectif_fk_3 FOREIGN KEY (idanneescolaire) REFERENCES boinae.anneescolaire(id)
);


-- boinae.eleve_join_competencecollectif definition

-- Drop table

-- DROP TABLE boinae.eleve_join_competencecollectif;

CREATE TABLE boinae.eleve_join_competencecollectif (
	ideleve int4 NULL,
	idcompetence int4 NULL,
	evaluation int4 NULL,
	autoevaluation int4 NULL,
	CONSTRAINT eleve_join_competencecollectif_fk FOREIGN KEY (ideleve) REFERENCES boinae.eleve(id),
	CONSTRAINT eleve_join_competencecollectif_fk_1 FOREIGN KEY (idcompetence) REFERENCES boinae.competence(id)
);


-- boinae.professeur_join_competence definition

-- Drop table

-- DROP TABLE boinae.professeur_join_competence;

CREATE TABLE boinae.professeur_join_competence (
	idprofesseur int4 NULL,
	idcompetence int4 NULL,
	CONSTRAINT professeur_join_competence_fk FOREIGN KEY (idprofesseur) REFERENCES boinae.professeur(id),
	CONSTRAINT professeur_join_competence_fk_1 FOREIGN KEY (idcompetence) REFERENCES boinae.competence(id)
);
