CREATE SCHEMA IF NOT EXISTS moulinasconstric;

CREATE  TABLE IF NOT EXISTS moulinasconstric.competence ( 
	idcompetence         INT NOT NULL AUTO_INCREMENT    ,
	nomcompetence        varchar(45)    ,
	pratiquecompetence   varchar(45)    ,
	visiblecompetence    boolean DEFAULT true   ,
	CONSTRAINT pk_competence PRIMARY KEY ( idcompetence )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.`cycle` ( 
	idcycle              INT NOT NULL AUTO_INCREMENT  ,
	numerocycle          varchar(20) DEFAULT 1   ,
	anneecycle           varchar(20)    ,
	groupeoumono         boolean DEFAULT false comment '0=false=groupen1=true=mono-instrumental'   ,
	visiblecycle         boolean DEFAULT true   ,
	CONSTRAINT pk_cyclemono PRIMARY KEY ( idcycle )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.domaine ( 
	iddomaine            INT NOT NULL AUTO_INCREMENT  ,
	nomdomaine           varchar(45)    ,
	visibledomaine       boolean DEFAULT true   ,
	CONSTRAINT pk_domaine PRIMARY KEY ( iddomaine )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.domaine_join_competence ( 
	domaine_iddomaine    integer    ,
	competence_idcompetence integer    ,
	CONSTRAINT fk_domaine_join_competence_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES moulinasconstric.competence( idcompetence )   ,
	CONSTRAINT fk_domaine_join_competence_1 FOREIGN KEY ( domaine_iddomaine ) REFERENCES moulinasconstric.domaine( iddomaine )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.eleve ( 
	ideleve              INT NOT NULL AUTO_INCREMENT  ,
	nomeleve             varchar(45)    ,
	prenomeleve          varchar(45)    ,
	visibleeleve         boolean DEFAULT true   ,
	CONSTRAINT pk_eleve PRIMARY KEY ( ideleve )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.eleve_join_cycle ( 
	eleve_ideleve        integer    ,
	cycle_idcycle        integer    ,
	CONSTRAINT fk_eleve_join_cycle_2 FOREIGN KEY ( cycle_idcycle ) REFERENCES moulinasconstric.`cycle`( idcycle )   ,
	CONSTRAINT fk_eleve_join_cycle_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES moulinasconstric.eleve( ideleve )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.groupe ( 
	idgroupe             INT NOT NULL AUTO_INCREMENT  ,
	nomgroupe            varchar(45)    ,
	collectifouateliergroupe boolean DEFAULT false comment '0=false=collectifn1=true=groupe'   ,
	visiblegroupe        boolean DEFAULT true   ,
	CONSTRAINT pk_groupe PRIMARY KEY ( idgroupe )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.instrument ( 
	idinstrument         INT NOT NULL AUTO_INCREMENT  ,
	nominstrument        varchar(45)    ,
	visibleinstrument    boolean DEFAULT true   ,
	CONSTRAINT pk_instrument PRIMARY KEY ( idinstrument )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.professeur ( 
	idprofesseur         INT NOT NULL AUTO_INCREMENT  ,
	nomprofesseur        varchar(45)    ,
	prenomprofesseur     varchar(45)    ,
	visibleprofesseur    boolean DEFAULT true   ,
	CONSTRAINT pk_professeur PRIMARY KEY ( idprofesseur )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.professeur_join_groupe ( 
	professeur_idprofesseur integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_professeur_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES moulinasconstric.groupe( idgroupe )   ,
	CONSTRAINT fk_professeur_join_groupe_1 FOREIGN KEY ( professeur_idprofesseur ) REFERENCES moulinasconstric.professeur( idprofesseur )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.projet ( 
	idprojet             INT NOT NULL AUTO_INCREMENT  ,
	nomprojet            varchar(45)    ,
	datedebutprojet      varchar(10)    ,
	datefinprojet        varchar(10)    ,
	presentationprojet   longtext    ,
	bilanprojet          longtext    ,
	visibleprojet        boolean DEFAULT true   ,
	CONSTRAINT pk_projet PRIMARY KEY ( idprojet )
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.projet_join_competence ( 
	projet_idprojet      integer    ,
	competence_idcompetence integer    ,
	CONSTRAINT fk_projet_join_competence_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES moulinasconstric.competence( idcompetence )   ,
	CONSTRAINT fk_projet_join_competence_1 FOREIGN KEY ( projet_idprojet ) REFERENCES moulinasconstric.projet( idprojet )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.bilanelevemusiquecollective ( 
	idbilanelevemusiquecollective INT NOT NULL AUTO_INCREMENT  ,
	ideleve              integer    ,
	idgroupe             integer    ,
	idcompetence         integer    ,
	bilanbilanelevemusiquecollective longtext    ,
	statuscompetence     integer    ,
	datebilanelevemusiquecollective varchar(10)    ,
	CONSTRAINT pk_bilanelevemusiquecollective PRIMARY KEY ( idbilanelevemusiquecollective ),
	CONSTRAINT fk_bilanelevemusiquecollective_3 FOREIGN KEY ( idcompetence ) REFERENCES moulinasconstric.competence( idcompetence )   ,
	CONSTRAINT fk_bilanelevemusiquecollective_1 FOREIGN KEY ( ideleve ) REFERENCES moulinasconstric.eleve( ideleve )   ,
	CONSTRAINT fk_bilanelevemusiquecollective_2 FOREIGN KEY ( idgroupe ) REFERENCES moulinasconstric.groupe( idgroupe )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.eleve_join_groupe ( 
	eleve_ideleve        integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_eleve_join_groupe_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES moulinasconstric.eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES moulinasconstric.groupe( idgroupe )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.eleve_join_instrument ( 
	eleve_ideleve        integer    ,
	instrument_idinstrument integer    ,
	CONSTRAINT fk_eleve_join_instrument_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES moulinasconstric.eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_instrument_2 FOREIGN KEY ( instrument_idinstrument ) REFERENCES moulinasconstric.instrument( idinstrument )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.groupe_join_projet ( 
	groupe_idgroupe      integer    ,
	projet_idprojet      integer    ,
	CONSTRAINT fk_groupe_join_projet_1 FOREIGN KEY ( groupe_idgroupe ) REFERENCES moulinasconstric.groupe( idgroupe )   ,
	CONSTRAINT fk_groupe_join_projet_2 FOREIGN KEY ( projet_idprojet ) REFERENCES moulinasconstric.projet( idprojet )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.monoinstrumental ( 
	idmonoinstrumental   INT NOT NULL AUTO_INCREMENT  ,
	ideleve              integer    ,
	idprofesseur         integer    ,
	bilanmonoinstrumental longtext    ,
	datebilanmonoinstrumental varchar(10)    ,
	CONSTRAINT pk_monoinstrumental PRIMARY KEY ( idmonoinstrumental ),
	CONSTRAINT fk_monoinstrumental_1 FOREIGN KEY ( ideleve ) REFERENCES moulinasconstric.eleve( ideleve )   ,
	CONSTRAINT fk_monoinstrumental_2 FOREIGN KEY ( idprofesseur ) REFERENCES moulinasconstric.professeur( idprofesseur )   
 );

CREATE  TABLE IF NOT EXISTS moulinasconstric.evaluationcompetencemonoinstrumental ( 
	monoinstrumental_idmonoinstrumental integer    ,
	competence_idcompetence integer    ,
	statuscompetence     integer    ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES moulinasconstric.competence( idcompetence )   ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_1 FOREIGN KEY ( monoinstrumental_idmonoinstrumental ) REFERENCES moulinasconstric.monoinstrumental( idmonoinstrumental )   
 );

/* Moved to CREATE TABLE
COMMENT ON COLUMN moulinasconstric."cycle".groupeoumono IS '0=false=groupen1=true=mono-instrumental'; */

/* Moved to CREATE TABLE
COMMENT ON COLUMN moulinasconstric.groupe.collectifouateliergroupe IS '0=false=collectifn1=true=groupe'; */

