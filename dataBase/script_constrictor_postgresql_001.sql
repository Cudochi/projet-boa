CREATE SCHEMA IF NOT EXISTS contrictor;

CREATE  TABLE contrictor.bilanelevemusiquecollective ( 
	idbilanelevemusiquecollective serial  NOT NULL  ,
	ideleve              integer    ,
	idgroupe             integer    ,
	idcompetence         integer    ,
	bilanbilanelevemusiquecollective text    ,
	statuscompetence     integer    ,
	datebilanelevemusiquecollective varchar(10)    ,
	CONSTRAINT pk_bilanelevemusiquecollective PRIMARY KEY ( idbilanelevemusiquecollective )
 );

CREATE  TABLE contrictor.competence ( 
	idcompetence         serial  NOT NULL  ,
	nomcompetence        varchar(45)    ,
	pratiquecompetence   varchar(45)    ,
	visiblecompetence    boolean DEFAULT TRUE   ,
	CONSTRAINT pk_competence PRIMARY KEY ( idcompetence )
 );

CREATE  TABLE contrictor."cycle" ( 
	idcycle              serial  NOT NULL  ,
	numerocycle          varchar(20) DEFAULT 1   ,
	anneecycle           varchar(20)    ,
	groupeoumono         boolean DEFAULT FALSE   ,
	CONSTRAINT pk_cyclemono PRIMARY KEY ( idcycle )
 );

CREATE  TABLE contrictor.domaine ( 
	iddomaine            serial  NOT NULL  ,
	nomdomaine           varchar(45)    ,
	visibledomaine       boolean DEFAULT TRUE   ,
	CONSTRAINT pk_domaine PRIMARY KEY ( iddomaine )
 );

CREATE  TABLE contrictor.domaine_join_competence ( 
	domaine_iddomaine    integer    ,
	competence_idcompetence integer    ,
	CONSTRAINT fk_domaine_join_competence_1 FOREIGN KEY ( domaine_iddomaine ) REFERENCES contrictor.domaine( iddomaine )   ,
	CONSTRAINT fk_domaine_join_competence_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES contrictor.competence( idcompetence )   
 );

CREATE  TABLE contrictor.eleve ( 
	ideleve              serial  NOT NULL  ,
	nomeleve             varchar(45)    ,
	prenomeleve          varchar(45)    ,
	visibleeleve         boolean DEFAULT TRUE   ,
	CONSTRAINT pk_eleve PRIMARY KEY ( ideleve )
 );

CREATE  TABLE contrictor.eleve_join_cycle ( 
	eleve_ideleve        integer    ,
	cycle_idcycle        integer    ,
	CONSTRAINT fk_eleve_join_cycle_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES contrictor.eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_cycle_2 FOREIGN KEY ( cycle_idcycle ) REFERENCES contrictor."cycle"( idcycle )   
 );

CREATE  TABLE contrictor.groupe ( 
	idgroupe             serial  NOT NULL  ,
	nomgroupe            varchar(45)    ,
	collectifouateliergroupe boolean DEFAULT FALSE   ,
	CONSTRAINT pk_groupe PRIMARY KEY ( idgroupe )
 );

CREATE  TABLE contrictor.instrument ( 
	idinstrument         serial  NOT NULL  ,
	nominstrument        varchar(45)    ,
	visibleinstrument    boolean DEFAULT TRUE   ,
	CONSTRAINT pk_instrument PRIMARY KEY ( idinstrument )
 );

CREATE  TABLE contrictor.monoinstrumental ( 
	idmonoinstrumental   serial  NOT NULL  ,
	ideleve              integer    ,
	idprofesseur         integer    ,
	bilanmonoinstrumental text    ,
	datebilanmonoinstrumental varchar(10)    ,
	CONSTRAINT pk_monoinstrumental PRIMARY KEY ( idmonoinstrumental )
 );

CREATE  TABLE contrictor.professeur ( 
	idprofesseur         serial  NOT NULL  ,
	nomprofesseur        varchar(45)    ,
	prenomprofesseur     varchar(45)    ,
	visibleprofesseur    boolean DEFAULT TRUE   ,
	CONSTRAINT pk_professeur PRIMARY KEY ( idprofesseur )
 );

CREATE  TABLE contrictor.professeur_join_groupe ( 
	professeur_idprofesseur integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_professeur_join_groupe_1 FOREIGN KEY ( professeur_idprofesseur ) REFERENCES contrictor.professeur( idprofesseur )   ,
	CONSTRAINT fk_professeur_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES contrictor.groupe( idgroupe )   
 );

CREATE  TABLE contrictor.projet ( 
	idprojet             serial  NOT NULL  ,
	nomprojet            varchar(45)    ,
	datedebutprojet      date    ,
	datefinprojet        date    ,
	presentationprojet   text    ,
	bilanprojet          text    ,
	visibleprojet        boolean DEFAULT TRUE   ,
	CONSTRAINT pk_projet PRIMARY KEY ( idprojet )
 );

CREATE  TABLE contrictor.eleve_join_groupe ( 
	eleve_ideleve        integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_eleve_join_groupe_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES contrictor.eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES contrictor.groupe( idgroupe )   
 );

CREATE  TABLE contrictor.eleve_join_instrument ( 
	eleve_ideleve        integer    ,
	instrument_idinstrument integer    ,
	CONSTRAINT fk_eleve_join_instrument_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES contrictor.eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_instrument_2 FOREIGN KEY ( instrument_idinstrument ) REFERENCES contrictor.instrument( idinstrument )   
 );

CREATE  TABLE contrictor.evaluationcompetencemonoinstrumentale ( 
	monoinstrumental_idmonoinstrumental integer    ,
	competence_idcompetence integer    ,
	statuscompetence     integer    ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_1 FOREIGN KEY ( monoinstrumental_idmonoinstrumental ) REFERENCES contrictor.monoinstrumental( idmonoinstrumental )   ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES contrictor.competence( idcompetence )   
 );

COMMENT ON COLUMN contrictor."cycle".groupeoumono IS '0=false=groupe\n1=true=mono-instrumental';

COMMENT ON COLUMN contrictor.groupe.collectifouateliergroupe IS '0=false=collectif\n1=true=groupe';

