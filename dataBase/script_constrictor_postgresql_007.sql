CREATE SCHEMA IF NOT EXISTS constrictor;

CREATE SEQUENCE constrictor.bilanelevemusiquecollective_idbilanelevemusiquecollective_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.competence_idcompetence_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.cycle_idcycle_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.domaine_iddomaine_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.eleve_ideleve_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.groupe_idgroupe_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.instrument_idinstrument_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.monoinstrumental_idmonoinstrumental_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.professeur_idprofesseur_seq START WITH 1 INCREMENT BY 1;

CREATE SEQUENCE constrictor.projet_idprojet_seq START WITH 1 INCREMENT BY 1;

CREATE  TABLE constrictor.competence ( 
	idcompetence         integer DEFAULT nextval('constrictor.competence_idcompetence_seq'::regclass) NOT NULL  ,
	nomcompetence        varchar(45)    ,
	pratiquecompetence   varchar(45)    ,
	rubriquecompetence   varchar(45)    ,
	visiblecompetence    boolean DEFAULT true   ,
	CONSTRAINT pk_competence PRIMARY KEY ( idcompetence )
 );

CREATE  TABLE constrictor."cycle" ( 
	idcycle              integer DEFAULT nextval('constrictor.cycle_idcycle_seq'::regclass) NOT NULL  ,
	numerocycle          varchar(20) DEFAULT 1   ,
	anneecycle           varchar(20)    ,
	groupeoumono         boolean DEFAULT false   ,
	visiblecycle         boolean DEFAULT true   ,
	CONSTRAINT pk_cyclemono PRIMARY KEY ( idcycle )
 );

CREATE  TABLE constrictor.domaine ( 
	iddomaine            integer DEFAULT nextval('constrictor.domaine_iddomaine_seq'::regclass) NOT NULL  ,
	nomdomaine           varchar(45)    ,
	visibledomaine       boolean DEFAULT true   ,
	CONSTRAINT pk_domaine PRIMARY KEY ( iddomaine )
 );

CREATE  TABLE constrictor.domaine_join_competence ( 
	domaine_iddomaine    integer    ,
	competence_idcompetence integer    ,
	CONSTRAINT fk_domaine_join_competence_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES constrictor.competence( idcompetence )   ,
	CONSTRAINT fk_domaine_join_competence_1 FOREIGN KEY ( domaine_iddomaine ) REFERENCES constrictor.domaine( iddomaine )   
 );

CREATE  TABLE constrictor.eleve ( 
	ideleve              integer DEFAULT nextval('constrictor.eleve_ideleve_seq'::regclass) NOT NULL  ,
	nomeleve             varchar(45)    ,
	prenomeleve          varchar(45)    ,
	visibleeleve         boolean DEFAULT true   ,
	CONSTRAINT pk_eleve PRIMARY KEY ( ideleve )
 );

CREATE  TABLE constrictor.eleve_join_cycle ( 
	eleve_ideleve        integer    ,
	cycle_idcycle        integer    ,
	CONSTRAINT fk_eleve_join_cycle_2 FOREIGN KEY ( cycle_idcycle ) REFERENCES constrictor."cycle"( idcycle )   ,
	CONSTRAINT fk_eleve_join_cycle_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES constrictor.eleve( ideleve )   
 );

CREATE  TABLE constrictor.groupe ( 
	idgroupe             integer DEFAULT nextval('constrictor.groupe_idgroupe_seq'::regclass) NOT NULL  ,
	nomgroupe            varchar(45)    ,
	collectifouateliergroupe boolean DEFAULT false   ,
	visiblegroupe        boolean DEFAULT true   ,
	CONSTRAINT pk_groupe PRIMARY KEY ( idgroupe )
 );

CREATE  TABLE constrictor.instrument ( 
	idinstrument         integer DEFAULT nextval('constrictor.instrument_idinstrument_seq'::regclass) NOT NULL  ,
	nominstrument        varchar(45)    ,
	visibleinstrument    boolean DEFAULT true   ,
	CONSTRAINT pk_instrument PRIMARY KEY ( idinstrument )
 );

CREATE  TABLE constrictor.professeur ( 
	idprofesseur         integer DEFAULT nextval('constrictor.professeur_idprofesseur_seq'::regclass) NOT NULL  ,
	nomprofesseur        varchar(45)    ,
	prenomprofesseur     varchar(45)    ,
	visibleprofesseur    boolean DEFAULT true   ,
	CONSTRAINT pk_professeur PRIMARY KEY ( idprofesseur )
 );

CREATE  TABLE constrictor.professeur_join_groupe ( 
	professeur_idprofesseur integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_professeur_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES constrictor.groupe( idgroupe )   ,
	CONSTRAINT fk_professeur_join_groupe_1 FOREIGN KEY ( professeur_idprofesseur ) REFERENCES constrictor.professeur( idprofesseur )   
 );

CREATE  TABLE constrictor.projet ( 
	idprojet             integer DEFAULT nextval('constrictor.projet_idprojet_seq'::regclass) NOT NULL  ,
	nomprojet            varchar(45)    ,
	datedebutprojet      varchar(10)    ,
	datefinprojet        varchar(10)    ,
	presentationprojet   text    ,
	bilanprojet          text    ,
	visibleprojet        boolean DEFAULT true   ,
	CONSTRAINT pk_projet PRIMARY KEY ( idprojet )
 );

CREATE  TABLE constrictor.projet_join_competence ( 
	projet_idprojet      integer    ,
	competence_idcompetence integer    ,
	CONSTRAINT fk_projet_join_competence_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES constrictor.competence( idcompetence )   ,
	CONSTRAINT fk_projet_join_competence_1 FOREIGN KEY ( projet_idprojet ) REFERENCES constrictor.projet( idprojet )   
 );

CREATE  TABLE constrictor.bilanelevemusiquecollective ( 
	idbilanelevemusiquecollective integer DEFAULT nextval('constrictor.bilanelevemusiquecollective_idbilanelevemusiquecollective_seq'::regclass) NOT NULL  ,
	ideleve              integer    ,
	idgroupe             integer    ,
	idcompetence         integer    ,
	bilanbilanelevemusiquecollective text    ,
	statuscompetence     integer    ,
	datebilanelevemusiquecollective varchar(10)    ,
	CONSTRAINT pk_bilanelevemusiquecollective PRIMARY KEY ( idbilanelevemusiquecollective ),
	CONSTRAINT fk_bilanelevemusiquecollective_3 FOREIGN KEY ( idcompetence ) REFERENCES constrictor.competence( idcompetence )   ,
	CONSTRAINT fk_bilanelevemusiquecollective_1 FOREIGN KEY ( ideleve ) REFERENCES constrictor.eleve( ideleve )   ,
	CONSTRAINT fk_bilanelevemusiquecollective_2 FOREIGN KEY ( idgroupe ) REFERENCES constrictor.groupe( idgroupe )   
 );

CREATE  TABLE constrictor.eleve_join_groupe ( 
	eleve_ideleve        integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_eleve_join_groupe_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES constrictor.eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES constrictor.groupe( idgroupe )   
 );

CREATE  TABLE constrictor.eleve_join_instrument ( 
	eleve_ideleve        integer    ,
	instrument_idinstrument integer    ,
	CONSTRAINT fk_eleve_join_instrument_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES constrictor.eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_instrument_2 FOREIGN KEY ( instrument_idinstrument ) REFERENCES constrictor.instrument( idinstrument )   
 );

CREATE  TABLE constrictor.groupe_join_projet ( 
	groupe_idgroupe      integer    ,
	projet_idprojet      integer    ,
	CONSTRAINT fk_groupe_join_projet_1 FOREIGN KEY ( groupe_idgroupe ) REFERENCES constrictor.groupe( idgroupe )   ,
	CONSTRAINT fk_groupe_join_projet_2 FOREIGN KEY ( projet_idprojet ) REFERENCES constrictor.projet( idprojet )   
 );

CREATE  TABLE constrictor.monoinstrumental ( 
	idmonoinstrumental   integer DEFAULT nextval('constrictor.monoinstrumental_idmonoinstrumental_seq'::regclass) NOT NULL  ,
	ideleve              integer    ,
	idprofesseur         integer    ,
	bilanmonoinstrumental text    ,
	datebilanmonoinstrumental varchar(10)    ,
	CONSTRAINT pk_monoinstrumental PRIMARY KEY ( idmonoinstrumental ),
	CONSTRAINT fk_monoinstrumental_1 FOREIGN KEY ( ideleve ) REFERENCES constrictor.eleve( ideleve )   ,
	CONSTRAINT fk_monoinstrumental_2 FOREIGN KEY ( idprofesseur ) REFERENCES constrictor.professeur( idprofesseur )   
 );

CREATE  TABLE constrictor.evaluationcompetencemonoinstrumental ( 
	monoinstrumental_idmonoinstrumental integer    ,
	competence_idcompetence integer    ,
	statuscompetence     integer    ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES constrictor.competence( idcompetence )   ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_1 FOREIGN KEY ( monoinstrumental_idmonoinstrumental ) REFERENCES constrictor.monoinstrumental( idmonoinstrumental )   
 );

COMMENT ON COLUMN constrictor."cycle".groupeoumono IS '0=false=groupe\n1=true=mono-instrumental';

COMMENT ON COLUMN constrictor.groupe.collectifouateliergroupe IS '0=false=collectif\n1=true=groupe';

