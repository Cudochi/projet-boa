--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11 (Ubuntu 14.11-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.11 (Ubuntu 14.11-0ubuntu0.22.04.1)

-- Started on 2024-03-30 18:45:37 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE IF EXISTS constrictor;
--
-- TOC entry 3505 (class 1262 OID 16385)
-- Name: constrictor; Type: DATABASE; Schema: -; Owner: kavan
--

CREATE DATABASE constrictor WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'fr_FR.UTF-8';


ALTER DATABASE constrictor OWNER TO kavan;

\connect constrictor

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16606)
-- Name: constrictor; Type: SCHEMA; Schema: -; Owner: kavan
--

CREATE SCHEMA constrictor;


ALTER SCHEMA constrictor OWNER TO kavan;

--
-- TOC entry 210 (class 1259 OID 16607)
-- Name: bilanelevemusiquecollective_idbilanelevemusiquecollective_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.bilanelevemusiquecollective_idbilanelevemusiquecollective_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.bilanelevemusiquecollective_idbilanelevemusiquecollective_seq OWNER TO kavan;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 232 (class 1259 OID 16730)
-- Name: bilanelevemusiquecollective; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.bilanelevemusiquecollective (
    idbilanelevemusiquecollective integer DEFAULT nextval('constrictor.bilanelevemusiquecollective_idbilanelevemusiquecollective_seq'::regclass) NOT NULL,
    ideleve integer,
    idgroupe integer,
    idcompetence integer,
    bilanbilanelevemusiquecollective text,
    statuscompetence integer,
    datebilanelevemusiquecollective character varying(10)
);


ALTER TABLE constrictor.bilanelevemusiquecollective OWNER TO kavan;

--
-- TOC entry 211 (class 1259 OID 16608)
-- Name: competence_idcompetence_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.competence_idcompetence_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.competence_idcompetence_seq OWNER TO kavan;

--
-- TOC entry 220 (class 1259 OID 16617)
-- Name: competence; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.competence (
    idcompetence integer DEFAULT nextval('constrictor.competence_idcompetence_seq'::regclass) NOT NULL,
    nomcompetence character varying(45),
    pratiquecompetence character varying(45),
    visiblecompetence boolean DEFAULT true,
    rubriquecompetence character varying(45)[]
);


ALTER TABLE constrictor.competence OWNER TO kavan;

--
-- TOC entry 212 (class 1259 OID 16609)
-- Name: cycle_idcycle_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.cycle_idcycle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.cycle_idcycle_seq OWNER TO kavan;

--
-- TOC entry 221 (class 1259 OID 16624)
-- Name: cycle; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.cycle (
    idcycle integer DEFAULT nextval('constrictor.cycle_idcycle_seq'::regclass) NOT NULL,
    numerocycle character varying(20) DEFAULT 1,
    anneecycle character varying(20),
    groupeoumono boolean DEFAULT false,
    visiblecycle boolean DEFAULT true
);


ALTER TABLE constrictor.cycle OWNER TO kavan;

--
-- TOC entry 3506 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN cycle.groupeoumono; Type: COMMENT; Schema: constrictor; Owner: kavan
--

COMMENT ON COLUMN constrictor.cycle.groupeoumono IS '0=false=groupe\n1=true=mono-instrumental';


--
-- TOC entry 213 (class 1259 OID 16610)
-- Name: domaine_iddomaine_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.domaine_iddomaine_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.domaine_iddomaine_seq OWNER TO kavan;

--
-- TOC entry 222 (class 1259 OID 16633)
-- Name: domaine; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.domaine (
    iddomaine integer DEFAULT nextval('constrictor.domaine_iddomaine_seq'::regclass) NOT NULL,
    nomdomaine character varying(45),
    visibledomaine boolean DEFAULT true
);


ALTER TABLE constrictor.domaine OWNER TO kavan;

--
-- TOC entry 223 (class 1259 OID 16640)
-- Name: domaine_join_competence; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.domaine_join_competence (
    domaine_iddomaine integer,
    competence_idcompetence integer
);


ALTER TABLE constrictor.domaine_join_competence OWNER TO kavan;

--
-- TOC entry 214 (class 1259 OID 16611)
-- Name: eleve_ideleve_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.eleve_ideleve_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.eleve_ideleve_seq OWNER TO kavan;

--
-- TOC entry 224 (class 1259 OID 16653)
-- Name: eleve; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.eleve (
    ideleve integer DEFAULT nextval('constrictor.eleve_ideleve_seq'::regclass) NOT NULL,
    nomeleve character varying(45),
    prenomeleve character varying(45),
    visibleeleve boolean DEFAULT true
);


ALTER TABLE constrictor.eleve OWNER TO kavan;

--
-- TOC entry 238 (class 1259 OID 16823)
-- Name: eleve_evaluation_competence; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.eleve_evaluation_competence (
    ideleve integer,
    idcompetence integer,
    evaluation integer,
    autoevaluation integer
);


ALTER TABLE constrictor.eleve_evaluation_competence OWNER TO kavan;

--
-- TOC entry 225 (class 1259 OID 16660)
-- Name: eleve_join_cycle; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.eleve_join_cycle (
    eleve_ideleve integer,
    cycle_idcycle integer
);


ALTER TABLE constrictor.eleve_join_cycle OWNER TO kavan;

--
-- TOC entry 233 (class 1259 OID 16753)
-- Name: eleve_join_groupe; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.eleve_join_groupe (
    eleve_ideleve integer,
    groupe_idgroupe integer
);


ALTER TABLE constrictor.eleve_join_groupe OWNER TO kavan;

--
-- TOC entry 234 (class 1259 OID 16766)
-- Name: eleve_join_instrument; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.eleve_join_instrument (
    eleve_ideleve integer,
    instrument_idinstrument integer
);


ALTER TABLE constrictor.eleve_join_instrument OWNER TO kavan;

--
-- TOC entry 237 (class 1259 OID 16810)
-- Name: evaluationcompetencemonoinstrumental; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.evaluationcompetencemonoinstrumental (
    monoinstrumental_idmonoinstrumental integer,
    competence_idcompetence integer,
    statuscompetence integer
);


ALTER TABLE constrictor.evaluationcompetencemonoinstrumental OWNER TO kavan;

--
-- TOC entry 215 (class 1259 OID 16612)
-- Name: groupe_idgroupe_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.groupe_idgroupe_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.groupe_idgroupe_seq OWNER TO kavan;

--
-- TOC entry 226 (class 1259 OID 16673)
-- Name: groupe; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.groupe (
    idgroupe integer DEFAULT nextval('constrictor.groupe_idgroupe_seq'::regclass) NOT NULL,
    nomgroupe character varying(45),
    collectifouateliergroupe boolean DEFAULT false,
    visiblegroupe boolean DEFAULT true
);


ALTER TABLE constrictor.groupe OWNER TO kavan;

--
-- TOC entry 3507 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN groupe.collectifouateliergroupe; Type: COMMENT; Schema: constrictor; Owner: kavan
--

COMMENT ON COLUMN constrictor.groupe.collectifouateliergroupe IS '0=false=collectif\n1=true=groupe';


--
-- TOC entry 235 (class 1259 OID 16779)
-- Name: groupe_join_projet; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.groupe_join_projet (
    groupe_idgroupe integer,
    projet_idprojet integer
);


ALTER TABLE constrictor.groupe_join_projet OWNER TO kavan;

--
-- TOC entry 216 (class 1259 OID 16613)
-- Name: instrument_idinstrument_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.instrument_idinstrument_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.instrument_idinstrument_seq OWNER TO kavan;

--
-- TOC entry 227 (class 1259 OID 16681)
-- Name: instrument; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.instrument (
    idinstrument integer DEFAULT nextval('constrictor.instrument_idinstrument_seq'::regclass) NOT NULL,
    nominstrument character varying(45),
    visibleinstrument boolean DEFAULT true
);


ALTER TABLE constrictor.instrument OWNER TO kavan;

--
-- TOC entry 217 (class 1259 OID 16614)
-- Name: monoinstrumental_idmonoinstrumental_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.monoinstrumental_idmonoinstrumental_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.monoinstrumental_idmonoinstrumental_seq OWNER TO kavan;

--
-- TOC entry 236 (class 1259 OID 16792)
-- Name: monoinstrumental; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.monoinstrumental (
    idmonoinstrumental integer DEFAULT nextval('constrictor.monoinstrumental_idmonoinstrumental_seq'::regclass) NOT NULL,
    ideleve integer,
    idprofesseur integer,
    bilanmonoinstrumental text,
    datebilanmonoinstrumental character varying(10)
);


ALTER TABLE constrictor.monoinstrumental OWNER TO kavan;

--
-- TOC entry 218 (class 1259 OID 16615)
-- Name: professeur_idprofesseur_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.professeur_idprofesseur_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.professeur_idprofesseur_seq OWNER TO kavan;

--
-- TOC entry 228 (class 1259 OID 16688)
-- Name: professeur; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.professeur (
    idprofesseur integer DEFAULT nextval('constrictor.professeur_idprofesseur_seq'::regclass) NOT NULL,
    nomprofesseur character varying(45),
    prenomprofesseur character varying(45),
    visibleprofesseur boolean DEFAULT true
);


ALTER TABLE constrictor.professeur OWNER TO kavan;

--
-- TOC entry 229 (class 1259 OID 16695)
-- Name: professeur_join_groupe; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.professeur_join_groupe (
    professeur_idprofesseur integer,
    groupe_idgroupe integer
);


ALTER TABLE constrictor.professeur_join_groupe OWNER TO kavan;

--
-- TOC entry 219 (class 1259 OID 16616)
-- Name: projet_idprojet_seq; Type: SEQUENCE; Schema: constrictor; Owner: kavan
--

CREATE SEQUENCE constrictor.projet_idprojet_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE constrictor.projet_idprojet_seq OWNER TO kavan;

--
-- TOC entry 230 (class 1259 OID 16708)
-- Name: projet; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.projet (
    idprojet integer DEFAULT nextval('constrictor.projet_idprojet_seq'::regclass) NOT NULL,
    nomprojet character varying(45),
    datedebutprojet character varying(10),
    datefinprojet character varying(10),
    presentationprojet text,
    bilanprojet text,
    visibleprojet boolean DEFAULT true
);


ALTER TABLE constrictor.projet OWNER TO kavan;

--
-- TOC entry 231 (class 1259 OID 16717)
-- Name: projet_join_competence; Type: TABLE; Schema: constrictor; Owner: kavan
--

CREATE TABLE constrictor.projet_join_competence (
    projet_idprojet integer,
    competence_idcompetence integer
);


ALTER TABLE constrictor.projet_join_competence OWNER TO kavan;

--
-- TOC entry 3335 (class 2606 OID 16737)
-- Name: bilanelevemusiquecollective pk_bilanelevemusiquecollective; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.bilanelevemusiquecollective
    ADD CONSTRAINT pk_bilanelevemusiquecollective PRIMARY KEY (idbilanelevemusiquecollective);


--
-- TOC entry 3319 (class 2606 OID 16623)
-- Name: competence pk_competence; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.competence
    ADD CONSTRAINT pk_competence PRIMARY KEY (idcompetence);


--
-- TOC entry 3321 (class 2606 OID 16632)
-- Name: cycle pk_cyclemono; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.cycle
    ADD CONSTRAINT pk_cyclemono PRIMARY KEY (idcycle);


--
-- TOC entry 3323 (class 2606 OID 16639)
-- Name: domaine pk_domaine; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.domaine
    ADD CONSTRAINT pk_domaine PRIMARY KEY (iddomaine);


--
-- TOC entry 3325 (class 2606 OID 16659)
-- Name: eleve pk_eleve; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve
    ADD CONSTRAINT pk_eleve PRIMARY KEY (ideleve);


--
-- TOC entry 3327 (class 2606 OID 16680)
-- Name: groupe pk_groupe; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.groupe
    ADD CONSTRAINT pk_groupe PRIMARY KEY (idgroupe);


--
-- TOC entry 3329 (class 2606 OID 16687)
-- Name: instrument pk_instrument; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.instrument
    ADD CONSTRAINT pk_instrument PRIMARY KEY (idinstrument);


--
-- TOC entry 3337 (class 2606 OID 16799)
-- Name: monoinstrumental pk_monoinstrumental; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.monoinstrumental
    ADD CONSTRAINT pk_monoinstrumental PRIMARY KEY (idmonoinstrumental);


--
-- TOC entry 3331 (class 2606 OID 16694)
-- Name: professeur pk_professeur; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.professeur
    ADD CONSTRAINT pk_professeur PRIMARY KEY (idprofesseur);


--
-- TOC entry 3333 (class 2606 OID 16716)
-- Name: projet pk_projet; Type: CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.projet
    ADD CONSTRAINT pk_projet PRIMARY KEY (idprojet);


--
-- TOC entry 3347 (class 2606 OID 16743)
-- Name: bilanelevemusiquecollective fk_bilanelevemusiquecollective_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.bilanelevemusiquecollective
    ADD CONSTRAINT fk_bilanelevemusiquecollective_1 FOREIGN KEY (ideleve) REFERENCES constrictor.eleve(ideleve);


--
-- TOC entry 3348 (class 2606 OID 16748)
-- Name: bilanelevemusiquecollective fk_bilanelevemusiquecollective_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.bilanelevemusiquecollective
    ADD CONSTRAINT fk_bilanelevemusiquecollective_2 FOREIGN KEY (idgroupe) REFERENCES constrictor.groupe(idgroupe);


--
-- TOC entry 3346 (class 2606 OID 16738)
-- Name: bilanelevemusiquecollective fk_bilanelevemusiquecollective_3; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.bilanelevemusiquecollective
    ADD CONSTRAINT fk_bilanelevemusiquecollective_3 FOREIGN KEY (idcompetence) REFERENCES constrictor.competence(idcompetence);


--
-- TOC entry 3339 (class 2606 OID 16648)
-- Name: domaine_join_competence fk_domaine_join_competence_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.domaine_join_competence
    ADD CONSTRAINT fk_domaine_join_competence_1 FOREIGN KEY (domaine_iddomaine) REFERENCES constrictor.domaine(iddomaine);


--
-- TOC entry 3338 (class 2606 OID 16643)
-- Name: domaine_join_competence fk_domaine_join_competence_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.domaine_join_competence
    ADD CONSTRAINT fk_domaine_join_competence_2 FOREIGN KEY (competence_idcompetence) REFERENCES constrictor.competence(idcompetence);


--
-- TOC entry 3360 (class 2606 OID 16831)
-- Name: eleve_evaluation_competence fk_eleve_evaluation_competence_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_evaluation_competence
    ADD CONSTRAINT fk_eleve_evaluation_competence_1 FOREIGN KEY (ideleve) REFERENCES constrictor.eleve(ideleve);


--
-- TOC entry 3359 (class 2606 OID 16826)
-- Name: eleve_evaluation_competence fk_eleve_evaluation_competence_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_evaluation_competence
    ADD CONSTRAINT fk_eleve_evaluation_competence_2 FOREIGN KEY (idcompetence) REFERENCES constrictor.competence(idcompetence);


--
-- TOC entry 3341 (class 2606 OID 16668)
-- Name: eleve_join_cycle fk_eleve_join_cycle_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_join_cycle
    ADD CONSTRAINT fk_eleve_join_cycle_1 FOREIGN KEY (eleve_ideleve) REFERENCES constrictor.eleve(ideleve);


--
-- TOC entry 3340 (class 2606 OID 16663)
-- Name: eleve_join_cycle fk_eleve_join_cycle_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_join_cycle
    ADD CONSTRAINT fk_eleve_join_cycle_2 FOREIGN KEY (cycle_idcycle) REFERENCES constrictor.cycle(idcycle);


--
-- TOC entry 3349 (class 2606 OID 16756)
-- Name: eleve_join_groupe fk_eleve_join_groupe_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_join_groupe
    ADD CONSTRAINT fk_eleve_join_groupe_1 FOREIGN KEY (eleve_ideleve) REFERENCES constrictor.eleve(ideleve);


--
-- TOC entry 3350 (class 2606 OID 16761)
-- Name: eleve_join_groupe fk_eleve_join_groupe_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_join_groupe
    ADD CONSTRAINT fk_eleve_join_groupe_2 FOREIGN KEY (groupe_idgroupe) REFERENCES constrictor.groupe(idgroupe);


--
-- TOC entry 3351 (class 2606 OID 16769)
-- Name: eleve_join_instrument fk_eleve_join_instrument_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_join_instrument
    ADD CONSTRAINT fk_eleve_join_instrument_1 FOREIGN KEY (eleve_ideleve) REFERENCES constrictor.eleve(ideleve);


--
-- TOC entry 3352 (class 2606 OID 16774)
-- Name: eleve_join_instrument fk_eleve_join_instrument_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.eleve_join_instrument
    ADD CONSTRAINT fk_eleve_join_instrument_2 FOREIGN KEY (instrument_idinstrument) REFERENCES constrictor.instrument(idinstrument);


--
-- TOC entry 3358 (class 2606 OID 16818)
-- Name: evaluationcompetencemonoinstrumental fk_evaluationcompetencemonoinstrumentale_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.evaluationcompetencemonoinstrumental
    ADD CONSTRAINT fk_evaluationcompetencemonoinstrumentale_1 FOREIGN KEY (monoinstrumental_idmonoinstrumental) REFERENCES constrictor.monoinstrumental(idmonoinstrumental);


--
-- TOC entry 3357 (class 2606 OID 16813)
-- Name: evaluationcompetencemonoinstrumental fk_evaluationcompetencemonoinstrumentale_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.evaluationcompetencemonoinstrumental
    ADD CONSTRAINT fk_evaluationcompetencemonoinstrumentale_2 FOREIGN KEY (competence_idcompetence) REFERENCES constrictor.competence(idcompetence);


--
-- TOC entry 3353 (class 2606 OID 16782)
-- Name: groupe_join_projet fk_groupe_join_projet_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.groupe_join_projet
    ADD CONSTRAINT fk_groupe_join_projet_1 FOREIGN KEY (groupe_idgroupe) REFERENCES constrictor.groupe(idgroupe);


--
-- TOC entry 3354 (class 2606 OID 16787)
-- Name: groupe_join_projet fk_groupe_join_projet_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.groupe_join_projet
    ADD CONSTRAINT fk_groupe_join_projet_2 FOREIGN KEY (projet_idprojet) REFERENCES constrictor.projet(idprojet);


--
-- TOC entry 3355 (class 2606 OID 16800)
-- Name: monoinstrumental fk_monoinstrumental_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.monoinstrumental
    ADD CONSTRAINT fk_monoinstrumental_1 FOREIGN KEY (ideleve) REFERENCES constrictor.eleve(ideleve);


--
-- TOC entry 3356 (class 2606 OID 16805)
-- Name: monoinstrumental fk_monoinstrumental_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.monoinstrumental
    ADD CONSTRAINT fk_monoinstrumental_2 FOREIGN KEY (idprofesseur) REFERENCES constrictor.professeur(idprofesseur);


--
-- TOC entry 3343 (class 2606 OID 16703)
-- Name: professeur_join_groupe fk_professeur_join_groupe_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.professeur_join_groupe
    ADD CONSTRAINT fk_professeur_join_groupe_1 FOREIGN KEY (professeur_idprofesseur) REFERENCES constrictor.professeur(idprofesseur);


--
-- TOC entry 3342 (class 2606 OID 16698)
-- Name: professeur_join_groupe fk_professeur_join_groupe_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.professeur_join_groupe
    ADD CONSTRAINT fk_professeur_join_groupe_2 FOREIGN KEY (groupe_idgroupe) REFERENCES constrictor.groupe(idgroupe);


--
-- TOC entry 3345 (class 2606 OID 16725)
-- Name: projet_join_competence fk_projet_join_competence_1; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.projet_join_competence
    ADD CONSTRAINT fk_projet_join_competence_1 FOREIGN KEY (projet_idprojet) REFERENCES constrictor.projet(idprojet);


--
-- TOC entry 3344 (class 2606 OID 16720)
-- Name: projet_join_competence fk_projet_join_competence_2; Type: FK CONSTRAINT; Schema: constrictor; Owner: kavan
--

ALTER TABLE ONLY constrictor.projet_join_competence
    ADD CONSTRAINT fk_projet_join_competence_2 FOREIGN KEY (competence_idcompetence) REFERENCES constrictor.competence(idcompetence);


-- Completed on 2024-03-30 18:45:37 CET

--
-- PostgreSQL database dump complete
--

