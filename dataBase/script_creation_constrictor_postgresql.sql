CREATE SCHEMA IF NOT EXISTS "instance";

CREATE  TABLE "instance".bilanelevemusiquecollective ( 
	idbilanelevemusiquecollective integer[]  NOT NULL  ,
	ideleve              integer    ,
	idgroupe             integer    ,
	idcompetence         integer    ,
	bilanbilanelevemusiquecollective text    ,
	statuscompetence     integer    ,
	datebilanelevemusiquecollective varchar(10)    ,
	CONSTRAINT pk_bilanelevemusiquecollective PRIMARY KEY ( idbilanelevemusiquecollective )
 );

CREATE  TABLE "instance".competence ( 
	idcompetence         integer  NOT NULL  ,
	nomcompetence        varchar(45)    ,
	pratiquecompetence   varchar(45)    ,
	visiblecompetence    boolean DEFAULT TRUE   ,
	CONSTRAINT pk_competence PRIMARY KEY ( idcompetence )
 );

CREATE  TABLE "instance"."cycle" ( 
	idcycle              integer  NOT NULL  ,
	numerocycle          varchar(20) DEFAULT 1   ,
	anneecycle           varchar(20)    ,
	groupeoumono         boolean DEFAULT FALSE   ,
	CONSTRAINT pk_cyclemono PRIMARY KEY ( idcycle )
 );

CREATE  TABLE "instance".domaine ( 
	iddomaine            integer  NOT NULL  ,
	nomdomaine           varchar(45)    ,
	visibledomaine       boolean DEFAULT TRUE   ,
	CONSTRAINT pk_domaine PRIMARY KEY ( iddomaine )
 );

CREATE  TABLE "instance".domaine_join_competence ( 
	domaine_iddomaine    integer    ,
	competence_idcompetence integer    ,
	CONSTRAINT fk_domaine_join_competence_1 FOREIGN KEY ( domaine_iddomaine ) REFERENCES "instance".domaine( iddomaine )   ,
	CONSTRAINT fk_domaine_join_competence_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES "instance".competence( idcompetence )   
 );

CREATE  TABLE "instance".eleve ( 
	ideleve              integer  NOT NULL  ,
	nomeleve             varchar(45)    ,
	prenomeleve          varchar(45)    ,
	visibleeleve         boolean DEFAULT TRUE   ,
	CONSTRAINT pk_eleve PRIMARY KEY ( ideleve )
 );

CREATE  TABLE "instance".eleve_join_cycle ( 
	eleve_ideleve        integer    ,
	cycle_idcycle        integer    ,
	CONSTRAINT fk_eleve_join_cycle_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES "instance".eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_cycle_2 FOREIGN KEY ( cycle_idcycle ) REFERENCES "instance"."cycle"( idcycle )   
 );

CREATE  TABLE "instance".groupe ( 
	idgroupe             integer  NOT NULL  ,
	nomgroupe            varchar(45)    ,
	collectifouateliergroupe boolean DEFAULT FALSE   ,
	CONSTRAINT pk_groupe PRIMARY KEY ( idgroupe )
 );

CREATE  TABLE "instance".instrument ( 
	idinstrument         integer  NOT NULL  ,
	nominstrument        varchar(45)    ,
	visibleinstrument    boolean DEFAULT TRUE   ,
	CONSTRAINT pk_instrument PRIMARY KEY ( idinstrument )
 );

CREATE  TABLE "instance".monoinstrumental ( 
	idmonoinstrumental   integer  NOT NULL  ,
	ideleve              integer    ,
	idprofesseur         integer    ,
	bilanmonoinstrumental text    ,
	datebilanmonoinstrumental varchar(10)    ,
	CONSTRAINT pk_monoinstrumental PRIMARY KEY ( idmonoinstrumental )
 );

CREATE  TABLE "instance".professeur ( 
	idprofesseur         integer  NOT NULL  ,
	nomprofesseur        varchar(45)    ,
	prenomprofesseur     varchar(45)    ,
	visibleprofesseur    boolean DEFAULT TRUE   ,
	CONSTRAINT pk_professeur PRIMARY KEY ( idprofesseur )
 );

CREATE  TABLE "instance".professeur_join_groupe ( 
	professeur_idprofesseur integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_professeur_join_groupe_1 FOREIGN KEY ( professeur_idprofesseur ) REFERENCES "instance".professeur( idprofesseur )   ,
	CONSTRAINT fk_professeur_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES "instance".groupe( idgroupe )   
 );

CREATE  TABLE "instance".projet ( 
	idprojet             integer  NOT NULL  ,
	nomprojet            varchar(45)    ,
	datedebutprojet      date    ,
	datefinprojet        date    ,
	presentationprojet   text    ,
	bilanprojet          text    ,
	visibleprojet        boolean DEFAULT TRUE   ,
	CONSTRAINT pk_projet PRIMARY KEY ( idprojet )
 );

CREATE  TABLE "instance".eleve_join_groupe ( 
	eleve_ideleve        integer    ,
	groupe_idgroupe      integer    ,
	CONSTRAINT fk_eleve_join_groupe_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES "instance".eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_groupe_2 FOREIGN KEY ( groupe_idgroupe ) REFERENCES "instance".groupe( idgroupe )   
 );

CREATE  TABLE "instance".eleve_join_instrument ( 
	eleve_ideleve        integer    ,
	instrument_idinstrument integer    ,
	CONSTRAINT fk_eleve_join_instrument_1 FOREIGN KEY ( eleve_ideleve ) REFERENCES "instance".eleve( ideleve )   ,
	CONSTRAINT fk_eleve_join_instrument_2 FOREIGN KEY ( instrument_idinstrument ) REFERENCES "instance".instrument( idinstrument )   
 );

CREATE  TABLE "instance".evaluationcompetencemonoinstrumentale ( 
	monoinstrumental_idmonoinstrumental integer    ,
	competence_idcompetence integer    ,
	statuscompetence     integer    ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_1 FOREIGN KEY ( monoinstrumental_idmonoinstrumental ) REFERENCES "instance".monoinstrumental( idmonoinstrumental )   ,
	CONSTRAINT fk_evaluationcompetencemonoinstrumentale_2 FOREIGN KEY ( competence_idcompetence ) REFERENCES "instance".competence( idcompetence )   
 );

COMMENT ON COLUMN "instance"."cycle".groupeoumono IS '0=false=groupe\n1=true=mono-instrumental';

COMMENT ON COLUMN "instance".groupe.collectifouateliergroupe IS '0=false=collectif\n1=true=groupe';

