import os
import uvicorn

if __name__ == "__main__":
    host = os.getenv("HOST", "0.0.0.0")
    port = os.getenv("PORT", 8080)
    uvicorn.run(app="src.main:app", host=host, port=port, reload=True, log_level="info", use_colors=True)
