#!/usr/bin/env python3

from dotenv import load_dotenv
import os

class envVarsGetter():
    try:
        load_dotenv()
        DATABASE_USER = os.getenv("DATABASE_USER")
        DATABASE_PASSWORD = os.getenv("DATABASE_PASSWORD")
        DATABASE_HOST = os.getenv("DATABASE_HOST")
        DATABASE_PORT = os.getenv("DATABASE_PORT")
        DATABASE_NAME = os.getenv("DATABASE_NAME")
        DATABASE_SSL = os.getenv("DATABASE_SSL")
        DATABASE_SCHEMA = os.getenv("DATABASE_SCHEMA")

        TABLE_BILANELEVEMUSIQUECOLLECTIVE = os.getenv("DATABASE_TABLE_BILANELEVEMUSIQUECOLLECTIVE")
        TABLE_COMPETENCE = os.getenv("DATABASE_TABLE_COMPETENCE")
        TABLE_CYCLE = os.getenv("DATABASE_TABLE_CYCLE")
        TABLE_DOMAINE = os.getenv("DATABASE_TABLE_DOMAINE")
        TABLE_DOMAINE_JOIN_COMPETENCE = os.getenv("DATABASE_TABLE_DOMAINE_JOIN_COMPETENCE")
        TABLE_ELEVE = os.getenv("DATABASE_TABLE_ELEVE")
        TABLE_ELEVE_JOIN_CYCLE = os.getenv("DATABASE_TABLE_ELEVE_JOIN_CYCLE")
        TABLE_ELEVE_JOIN_GROUPE = os.getenv("DATABASE_TABLE_ELEVE_JOIN_GROUPE")
        TABLE_ELEVE_JOIN_INSTRUMENT = os.getenv("DATABASE_TABLE_ELEVE_JOIN_INSTRUMENT")
        TABLE_EVALUATIONCOMPETENCEMONOINSTRUMENTAL = os.getenv("DATABASE_TABLE_EVALUATIONCOMPETENCEMONOINSTRUMENTAL")
        TABLE_GROUPE = os.getenv("DATABASE_TABLE_GROUPE")
        TABLE_GROUPE_JOIN_PROJET = os.getenv("DATABASE_TABLE_GROUPE_JOIN_PROJET")
        TABLE_INSTRUMENT = os.getenv("DATABASE_TABLE_INSTRUMENT")
        TABLE_MONOINSTRUMENTAL = os.getenv("DATABASE_TABLE_MONOINSTRUMENTAL")
        TABLE_PROFESSEUR = os.getenv("DATABASE_TABLE_PROFESSEUR")
        TABLE_PROFESSEUR_JOIN_GROUPE = os.getenv("DATABASE_TABLE_PROFESSEUR_JOIN_GROUPE")
        TABLE_PROJET = os.getenv("DATABASE_TABLE_PROJET")
        TABLE_PROJET_JOIN_COMPETENCE = os.getenv("DATABASE_TABLE_PROJET_JOIN_COMPETENCE")

        # USER = os.getenv("USER")
        # PASSWORD = os.getenv("PASSWORD")
        # TOKEN_URL = os.getenv("TOKEN_URL")
        # URL_GET_SERVICE_DATA = os.getenv("URL_GET_SERVICE_DATA")
        # SOFTWARE_ID = os.getenv("SOFTWARE_ID")
        # SEC_TOKEN = os.getenv("SEC_TOKEN")
        # DEBUG = os.getenv("DEBUG")

        # MAIL_ADDRESS = os.getenv("MAIL_ADDRESS")
        # MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
        # MAIL_SMTP_SERVER = os.getenv("MAIL_SMTP_SERVER")
        # MAIL_SMTP_PORT = os.getenv("MAIL_SMTP_PORT")

        # AZURE_STORAGE_ACCOUNT_KEY = os.getenv("AZURE_STORAGE_ACCOUNT_KEY")
        # AZURE_CONNECTION_CHAIN = os.getenv("AZURE_STORAGE_CONNECTION_STRING")

    except Exception as error:
        print ("Oops! An exception has occured:", error)
        print ("Exception TYPE:", type(error))
