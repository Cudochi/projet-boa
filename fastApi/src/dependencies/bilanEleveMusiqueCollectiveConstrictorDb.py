#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class BilanEleveMusiqueCollectiveConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListBilanEleveMusiqueCollective(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idBilanEleveMusiqueCollective = mview[index][rowCount]
            rowCount+=1
            idEleve = mview[index][rowCount]
            rowCount+=1
            idGroupe = mview[index][rowCount]
            rowCount+=1
            idCompetence = mview[index][rowCount]
            rowCount+=1
            bilanBilanEleveMusiqueCollective = mview[index][rowCount]
            rowCount+=1
            statusCompetence = mview[index][rowCount]
            rowCount+=1
            dateBilanEleveMusiqueCollective = mview[index][rowCount]
            output.append({
            	'Id Bilan Eleve Musique Collective': idBilanEleveMusiqueCollective,
            	'Id Eleve': idEleve,
            	'Id Groupe': idGroupe,
            	'Id Competence': idCompetence,
            	'Bilan': bilanBilanEleveMusiqueCollective,
                'Status Competence': statusCompetence,
                'Date Bilan': dateBilanEleveMusiqueCollective
            	})

        return output

    def getBilanEleveMusiqueCollectiveById(self, tableDb, idBilanEleveMusiqueCollective):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT * FROM {}.{} WHERE idbilanelevemusiquecollective = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idBilanEleveMusiqueCollective),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idBilanEleveMusiqueCollective = mview[rowCount]
        rowCount+=1
        idEleve = mview[rowCount]
        rowCount+=1
        idGroupe = mview[rowCount]
        rowCount+=1
        idCompetence = mview[rowCount]
        rowCount+=1
        bilanBilanEleveMusiqueCollective = mview[rowCount]
        rowCount+=1
        statusCompetence = mview[rowCount]
        rowCount+=1
        dateBilanEleveMusiqueCollective = mview[rowCount]
        output.append({
            'Id Bilan Eleve Musique Collective': idBilanEleveMusiqueCollective,
            'Id Eleve': idEleve,
            'Id Groupe': idGroupe,
            'Id Competence': idCompetence,
            'Bilan': bilanBilanEleveMusiqueCollective,
            'Status Competence': statusCompetence,
            'Date Bilan': dateBilanEleveMusiqueCollective
            })

        return output

    def insertBilanEleveMusiqueCollective(self, 
            tableDb, 
            idEleve, 
            idGroupe, 
            idCompetence, 
            bilanBilanEleveMusiqueCollective, 
            statusCompetence, 
            dateBilanEleveMusiqueCollective
            ):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(ideleve, idgroupe, idcompetence, bilanbilanelevemusiquecollective, statuscompetence, datebilanelevemusiquecollective) VALUES (%s, %s, %s, %s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), 
                str(idGroupe), 
                str(idCompetence), 
                bilanBilanEleveMusiqueCollective, 
                str(statusCompetence), 
                dateBilanEleveMusiqueCollective
                )
        )

        self.conn.commit()

    def deleteBilanEleveMusiqueCollective(self, tableDb, idBilanEleveMusiqueCollective):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idbilanelevemusiquecollective = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idBilanEleveMusiqueCollective),)
        )

        self.conn.commit()

    def updateBilanEleveMusiqueCollective(self, 
            tableDb, 
            idBilanEleveMusiqueCollective,
            idEleve, 
            idGroupe, 
            idCompetence, 
            bilanBilanEleveMusiqueCollective, 
            statusCompetence, 
            dateBilanEleveMusiqueCollective
            ):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getBilanEleveMusiqueCollectiveById(tableDb, idBilanEleveMusiqueCollective)
        
        if (dataJson == []):
            return False

        if (idBilanEleveMusiqueCollective == None):
            idBilanEleveMusiqueCollective = dataJson[0]["Id Bilan Musique Collective"]
        if (idEleve == None):
            idEleve = dataJson[0]["Id Eleve"]
        if (idGroupe == None):
            idGroupe = dataJson[0]["Id Groupe"]
        if (idCompetence == None):
            idCompetence = dataJson[0]["Id Competence"]
        if (bilanBilanEleveMusiqueCollective == None):
            bilanBilanEleveMusiqueCollective = dataJson[0]["Bilan"]
        if (statusCompetence == None):
            statusCompetence = dataJson[0]["Status Competence"]
        if (dateBilanEleveMusiqueCollective == None):
            dateBilanEleveMusiqueCollective = dataJson[0]["Date Bilan"]

        curs.execute(sql.SQL('UPDATE {}.{} SET idbilanelevemusiquecollective=%s, ideleve=%s, idgroupe=%s, idcompetence=%s, bilanbilanelevemusiquecollective=%s, statuscompetence=%s, datebilanelevemusiquecollective=%s WHERE idbilanelevemusiquecollective=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idBilanEleveMusiqueCollective),
                str(idEleve), 
                str(idGroupe), 
                str(idCompetence), 
                bilanBilanEleveMusiqueCollective, 
                str(statusCompetence), 
                dateBilanEleveMusiqueCollective,
                idBilanEleveMusiqueCollective
                )
            )

        self.conn.commit()

        return True
