#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class CompetenceConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListCompetence(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idCompetence = mview[index][rowCount]
            rowCount+=1
            nomCompetence = mview[index][rowCount]
            rowCount+=1
            pratiqueCompetence = mview[index][rowCount]
            rowCount+=1
            isVisibleCompetence = mview[index][rowCount]
            rowCount+=1
            rubriqueCompetence = mview[index][rowCount]
            output.append({
            	'Id': idCompetence,
            	'Nom': nomCompetence,
            	'Pratique': pratiqueCompetence,
                'Rubrique': rubriqueCompetence,
            	'Est Visible': isVisibleCompetence
            	})

        return output

    def getCompetenceById(self, tableDb, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT idcompetence, nomcompetence, pratiqueCompetence, rubriquecompetence, visiblecompetence FROM {}.{} WHERE idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCompetence),)
        )

        mview = curs.fetchone()

        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idCompetence = mview[rowCount]
        rowCount+=1
        nomCompetence = mview[rowCount]
        rowCount+=1
        pratiqueCompetence = mview[rowCount]
        rowCount+=1
        rubriqueCompetence = mview[rowCount]
        rowCount+=1
        isVisibleCompetence = mview[rowCount]
        output.append({
            'Id': idCompetence,
            'Nom': nomCompetence,
            'Pratique': pratiqueCompetence,
            'Rubrique': rubriqueCompetence,
            'Est Visible': isVisibleCompetence
            })

        return output

    def insertCompetence(self, tableDb, nomCompetence, pratiqueCompetence, rubriqueCompetence, isVisibleCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(nomcompetence, pratiquecompetence, rubriquecompetence, visiblecompetence) VALUES (%s, %s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomCompetence, pratiqueCompetence, rubriqueCompetence, isVisibleCompetence)
        )

        self.conn.commit()

    def deleteCompetence(self, tableDb, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCompetence),)
        )

        self.conn.commit()

    def updateCompetence(self, tableDb, idCompetence, nomCompetence, pratiqueCompetence, rubriqueCompetence, isVisibleCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        dataJson = self.getCompetenceById(tableDb, idCompetence)

        if (dataJson == []):
            return False

        if (nomCompetence == None):
            nomCompetence = dataJson[0]["Nom"]
        if (pratiqueCompetence == None):
            pratiqueCompetence = dataJson[0]["Pratique"]
        if (rubriqueCompetence == None):
            rubriqueCompetence = dataJson[0]["Rubrique"]
        if (isVisibleCompetence == None): 
            isVisibleCompetence = dataJson[0]["Est Visible"]

        curs.execute(sql.SQL('UPDATE {}.{} SET nomcompetence=%s, pratiqueCompetence=%s, rubriqueCompetence=%s, visiblecompetence=%s WHERE idcompetence=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomCompetence, pratiqueCompetence, rubriqueCompetence, isVisibleCompetence, idCompetence)
        )
        self.conn.commit()

        return True
