#!/usr/bin/env python3

from src.dependencies.eleveConstrictorDb import EleveConstrictor
from src.dependencies.professeurConstrictorDb import ProfesseurConstrictor 
from src.dependencies.groupeConstrictorDb import GroupeConstrictor 
from src.dependencies.instrumentConstrictorDb import InstrumentConstrictor
from src.dependencies.competenceConstrictorDb import CompetenceConstrictor 
from src.dependencies.domaineConstrictorDb import DomaineConstrictor 
from src.dependencies.bilanEleveMusiqueCollectiveConstrictorDb import BilanEleveMusiqueCollectiveConstrictor 
from src.dependencies.projetConstrictorDb import ProjetConstrictor
from src.dependencies.cycleConstrictorDb import CycleConstrictor
from src.dependencies.eleveJoinCycleConstrictorDb import EleveJoinCycleConstrictor
from src.dependencies.eleveJoinInstrumentConstrictorDb import EleveJoinInstrumentConstrictor
from src.dependencies.eleveJoinGroupeConstrictorDb import EleveJoinGroupeConstrictor
from src.dependencies.groupeJoinProjetConstrictorDb import GroupeJoinProjetConstrictor 
from src.dependencies.professeurJoinGroupeConstrictorDb import ProfesseurJoinGroupeConstrictor
from src.dependencies.projetJoinCompetenceConstrictorDb import ProjetJoinCompetenceConstrictor 
from src.dependencies.monoinstrumentalConstrictorDb import MonoinstrumentalConstrictor 
from src.dependencies.evaluationCompetenceMonoinstrumentalConstrictorDb import EvaluationCompetenceMonoinstrumentalConstrictor

class ConnectorConstrictor(EleveConstrictor,
                            ProfesseurConstrictor,
                            GroupeConstrictor,
                            InstrumentConstrictor,
                            CompetenceConstrictor,
                            DomaineConstrictor,
                            BilanEleveMusiqueCollectiveConstrictor,
                            ProjetConstrictor,
                            CycleConstrictor,
                            EleveJoinCycleConstrictor,
                            EleveJoinInstrumentConstrictor,
                            EleveJoinGroupeConstrictor,
                            GroupeJoinProjetConstrictor,
                            ProfesseurJoinGroupeConstrictor,
                            ProjetJoinCompetenceConstrictor,
                            MonoinstrumentalConstrictor,
                            EvaluationCompetenceMonoinstrumentalConstrictor
                            ):

    def __init__(self):
        super().__init__()

dbSession = ConnectorConstrictor()
dbSession.connectToDb()
