#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.psqlConnector import PsqlConnector

class ConstrictorDb(PsqlConnector):

    def __init__(self):
        super().__init__()

    def checkIfTableExist(self, tableDb, raiseException=True):
        """Check if the table called exists in the database or not.
        Args:
            tableDb: The name of the table containing the Id.
        Return:
            bool: Wether the row exists or not.
        """
        curs = self.cursor

        # curs.execute( sql.SQL("select exists(select * from information_schema.tables where table_name=%(tableDb)s);").format(sql.Identifier(tableDb)), {'tableDb': tableDb})
        curs.execute( sql.SQL("SELECT EXISTS ( SELECT FROM pg_tables WHERE tablename  =%(tableDb)s  );").format(sql.Identifier(tableDb)), {'tableDb': tableDb})

        if curs.fetchone()[0] == False:
            if raiseException == True:
                raise HTTPException(HTTP_404_NOT_FOUND)
            else:
                return False
        return True