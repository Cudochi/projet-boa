#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class CycleConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListCycle(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idCycle = mview[index][rowCount]
            rowCount+=1
            numeroCycle = mview[index][rowCount]
            rowCount+=1
            anneeCycle = mview[index][rowCount]
            rowCount+=1
            groupeOuMonoCycle = mview[index][rowCount]
            rowCount+=1
            isVisibleCycle = mview[index][rowCount]
            output.append({
            	'Id': idCycle,
            	'Numero': numeroCycle,
            	'Annee': anneeCycle,
                'Groupe ou Mono': groupeOuMonoCycle,
                'Visible': isVisibleCycle
            	})

        return output

    def getCycleById(self, tableDb, idCycle):
        if not isinstance(idCycle, int):
            return []
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT idcycle, numerocycle, anneecycle, groupeoumono, visiblecycle FROM {}.{} WHERE idcycle = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCycle),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idCycle = mview[rowCount]
        rowCount+=1
        numeroCycle = mview[rowCount]
        rowCount+=1
        anneeCycle = mview[rowCount]
        rowCount+=1
        groupeOuMonoCycle = mview[rowCount]
        rowCount+=1
        isVisibleCycle = mview[rowCount]
        output.append({
            'Id': idCycle,
            'Numero': numeroCycle,
            'Annee': anneeCycle,
            'Groupe ou Mono': groupeOuMonoCycle,
            'Visible': isVisibleCycle
            })

        return output

    def insertCycle(self, tableDb, numeroCycle, anneeCycle, groupeOuMonoCycle, isVisibleCycle):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(numerocycle, anneecycle, groupeoumono, visiblecycle) VALUES (%s, %s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (numeroCycle,
                anneeCycle,
                groupeOuMonoCycle,
                isVisibleCycle
                )
        )

        self.conn.commit()

    def deleteCycle(self, tableDb, idCycle):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idcycle = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCycle),)
        )

        self.conn.commit()

    def updateCycle(self, tableDb, idCycle, numeroCycle, anneeCycle, groupeOuMonoCycle, isVisibleCycle):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getCycleById(tableDb, idCycle)
        
        if (dataJson == []):
            return False

        if (idCycle== None):
            idCycle = dataJson[0]["Id"]
        if (numeroCycle == None): 
            numeroCycle = dataJson[0]["Numero"]
        if (anneeCycle == None): 
            anneeCycle = dataJson[0]["Annee"]
        if (groupeOuMonoCycle == None): 
            groupeOuMonoCycle = dataJson[0]["Groupe ou Mono"]
        if (isVisibleCycle == None): 
            isVisibleCycle = dataJson[0]["Visible"]

        curs.execute(sql.SQL('UPDATE {}.{} SET numerocycle=%s, anneecycle=%s, groupeoumono=%s, visiblecycle=%s WHERE idcycle=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (numeroCycle,
                anneeCycle,
                groupeOuMonoCycle,
                isVisibleCycle,
                str(idCycle))
        )
        self.conn.commit()

        return True
