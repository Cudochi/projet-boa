#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class DomaineConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListDomaine(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idDomaine = mview[index][rowCount]
            rowCount+=1
            nomDomaine = mview[index][rowCount]
            rowCount+=1
            isVisibleDomaine = mview[index][rowCount]
            output.append({
            	'Id': idDomaine,
            	'Nom': nomDomaine,
            	'Est Visible': isVisibleDomaine
            	})

        return output

    def getDomaineById(self, tableDb, idDomaine):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT iddomaine, nomdomaine, visibledomaine FROM {}.{} WHERE iddomaine = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idDomaine),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idDomaine = mview[rowCount]
        rowCount+=1
        nomDomaine = mview[rowCount]
        rowCount+=1
        isVisibleDomaine = mview[rowCount]
        output.append({
            'Id': idDomaine,
            'Nom': nomDomaine,
            'Est Visible': isVisibleDomaine
            })

        return output

    def insertDomaine(self, tableDb, nomDomaine, isVisibleDomaine):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(nomdomaine, visibledomaine) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomDomaine, isVisibleDomaine)
        )

        self.conn.commit()

    def deleteDomaine(self, tableDb, idDomaine):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE iddomaine = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idDomaine),)
        )

        self.conn.commit()

    def updateDomaine(self, tableDb, idDomaine, nomDomaine, isVisibleDomaine):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getDomaineById(tableDb, idDomaine)
        
        if (dataJson == []):
            return False

        if (nomDomaine == None):
            nomDomaine = dataJson[0]["Nom"]
        if (isVisibleDomaine == None): 
            isVisibleDomaine = dataJson[0]["Est Visible"]

        curs.execute(sql.SQL('UPDATE {}.{} SET nomdomaine=%s, visibledomaine=%s WHERE iddomaine=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomDomaine, isVisibleDomaine, idDomaine)
        )
        self.conn.commit()

        return True
