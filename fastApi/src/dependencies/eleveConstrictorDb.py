#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class EleveConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def test(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb, False)
        """ 
        curs.execute("SELECT datname FROM pg_database;")
        mview = curs.fetchall()
        
        print("mview : ")
        print(mview)
         """
        curs.execute("""
            SELECT table_catalog, table_schema, table_name
            FROM information_schema.tables
            WHERE table_type = 'BASE TABLE' AND table_schema NOT LIKE 'pg_%' AND table_schema != 'information_schema';
        """)
        rows = curs.fetchall()
        print(rows)

        # Process the query results
        for row in rows:
            table_catalog, table_schema, table_name = row
            print(f"{table_catalog}.{table_schema}.{table_name}")
            
        return True



    def getListEleve(self, tableDb):
        """Get the JSON list of the files currently inside the database.

        Returns:
           output: The JSON style list of the database's content.
        """
        curs = self.cursor

        self.checkIfTableExist(tableDb, False)

        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()
        # print(mview)
        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idEleve = mview[index][rowCount]
            rowCount+=1
            nomEleve = mview[index][rowCount]
            rowCount+=1
            prenomEleve = mview[index][rowCount]
            rowCount+=1
            isVisibleEleve = mview[index][rowCount]
            output.append({
            	'Id': idEleve,
            	'Nom': nomEleve,
            	'Prenom': prenomEleve,
            	'Est Visible': isVisibleEleve
            	})

        return output

    def getEleveById(self, tableDb, idEleve):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT ideleve, nomeleve, prenomeleve, visibleeleve FROM {}.{} WHERE ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        nomEleve = mview[rowCount]
        rowCount+=1
        prenomEleve = mview[rowCount]
        rowCount+=1
        isVisibleEleve = mview[rowCount]
        output.append({
            'Id': idEleve,
            'Nom': nomEleve,
            'Prenom': prenomEleve,
            'Est Visible': isVisibleEleve
            })

        return output

    def insertEleve(self, tableDb, nomEleve, prenomEleve, isVisibleEleve):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        insert_query = sql.SQL('INSERT INTO {}.{}(nomeleve, prenomeleve, visibleeleve) VALUES (%s, %s, %s) RETURNING ideleve;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )

        curs.execute(insert_query, (nomEleve, prenomEleve, isVisibleEleve))
        
        new_id = curs.fetchone()[0]  # Fetch the returned ID

        self.conn.commit()

        return new_id

    def deleteEleve(self, tableDb, idEleve):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        self.conn.commit()

    def updateEleve(self, tableDb, idEleve, nomEleve, prenomEleve, isVisibleEleve):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getEleveById(tableDb, idEleve)
        
        if (dataJson == []):
            return False

        if (nomEleve == None):
            nomEleve = dataJson[0]["Nom"]
        if (prenomEleve == None):
            prenomEleve = dataJson[0]["Prenom"]
        if (isVisibleEleve == None): 
            isVisibleEleve = dataJson[0]["Est Visible"]

        curs.execute(sql.SQL('UPDATE {}.{} SET nomeleve=%s, prenomeleve=%s, visibleeleve=%s WHERE ideleve=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomEleve, prenomEleve, isVisibleEleve, idEleve)
        )
        self.conn.commit()

        return True
