#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class EleveJoinCycleConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListEleveJoinCycle(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idEleve = mview[index][rowCount]
            rowCount+=1
            idCycle = mview[index][rowCount]
            output.append({
            	'Id Eleve': idEleve,
            	'Id Cycle': idCycle,
            	})

        return output

    def getEleveFromEleveJoinCycleById(self, tableDb, idEleve):
        if not isinstance(idEleve, int):
            return []
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT eleve_ideleve, cycle_idcycle FROM {}.{} WHERE eleve_ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idCycle = mview[rowCount]
        output.append({
            'Id Eleve': idEleve,
            'Id Cycle': idCycle,
            })

        return output

    def getCycleFromEleveJoinCycleById(self, tableDb, idCycle):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT cycle_idcycle, eleve_ideleve FROM {}.{} WHERE cycle_idcycle = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCycle),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idCycle = mview[rowCount]
        output.append({
            'Id Cycle': idCycle,
            'Id Eleve': idEleve,
            })

        return output

    def getPairFromEleveJoinCycleById(self, tableDb, idEleve, idCycle):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT eleve_ideleve, cycle_idcycle FROM {}.{} WHERE eleve_ideleve = %s AND cycle_idcycle = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), str(idCycle),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idCycle = mview[rowCount]
        output.append({
            'Id Eleve': idEleve,
            'Id Cycle': idCycle,
            })

        return output

    def insertEleveJoinCycle(self, tableDb, idEleve, idCycle):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(eleve_ideleve, cycle_idcycle) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idEleve, idCycle,)
        )

        self.conn.commit()

    def deleteEleveFromEleveJoinCycle(self, tableDb, idEleve):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE eleve_ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        self.conn.commit()

    def deleteCycleFromEleveJoinCycle(self, tableDb, idCycle):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE cycle_idcycle = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCycle),)
        )

        self.conn.commit()

    def deletePairFromEleveJoinCycle(self, tableDb, idEleve, idCycle):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE eleve_ideleve = %s AND cycle_idcycle = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), str(idCycle),)
        )

        self.conn.commit()

    def updateEleveJoinCycle(self, tableDb, idEleve, idCycle):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataEleveJson = self.getEleveFromEleveJoinCycleById(tableDb, idEleve)
        dataCycleJson = self.getCycleFromEleveJoinCycleById(tableDb, idCycle)
        
        if (dataEleveJson == []):
            return False
        if (dataCycleJson != []):
            return False

        if (idEleve == None):
            idEleve = dataEleveJson[0]["Id Eleve"]
        if (idCycle == None):
            idCycle = dataCycleJson[0]["Id Cycle"]

        curs.execute(sql.SQL('UPDATE {}.{} SET eleve_ideleve=%s, cycle_idcycle=%s WHERE eleve_ideleve=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idEleve, idCycle, idEleve)
        )
        self.conn.commit()

        return True
