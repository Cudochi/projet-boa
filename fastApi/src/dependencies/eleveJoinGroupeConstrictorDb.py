#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class EleveJoinGroupeConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListEleveJoinGroupe(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idEleve = mview[index][rowCount]
            rowCount+=1
            idGroupe = mview[index][rowCount]
            output.append({
            	'Id Eleve': idEleve,
            	'Id Groupe': idGroupe,
            	})

        return output

    def getEleveFromEleveJoinGroupeById(self, tableDb, idEleve):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT eleve_ideleve, groupe_idgroupe FROM {}.{} WHERE eleve_ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idGroupe = mview[rowCount]
        output.append({
            'Id Eleve': idEleve,
            'Id Groupe': idGroupe,
            })

        return output

    def getGroupeFromEleveJoinGroupeById(self, tableDb, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT groupe_idgroupe, eleve_ideleve FROM {}.{} WHERE groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        mview = curs.fetchall()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        for index in range(0,len(mview)):
            rowCount = 0
            idGroupe = mview[index][rowCount]
            rowCount+=1
            idEleve = mview[index][rowCount]
            output.append({
                'Id Groupe': idGroupe,
                'Id Eleve': idEleve,
                })

        return output

    def getPairFromEleveJoinGroupeById(self, tableDb, idEleve, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT eleve_ideleve, groupe_idgroupe FROM {}.{} WHERE eleve_ideleve = %s AND groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), str(idGroupe),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idGroupe = mview[rowCount]
        output.append({
            'Id Eleve': idEleve,
            'Id Groupe': idGroupe,
            })

        return output

    def insertEleveJoinGroupe(self, tableDb, idEleve, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(eleve_ideleve, groupe_idgroupe) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idEleve, idGroupe,)
        )

        self.conn.commit()

    def deleteEleveFromEleveJoinGroupe(self, tableDb, idEleve):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE eleve_ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        self.conn.commit()

    def deleteGroupeFromEleveJoinGroupe(self, tableDb, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        self.conn.commit()

    def deletePairFromEleveJoinGroupe(self, tableDb, idEleve, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE eleve_ideleve = %s AND groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), str(idGroupe),)
        )

        self.conn.commit()

    def updateEleveJoinGroupe(self, tableDb, idEleve, idGroupe):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataEleveJson = self.getEleveFromEleveJoinGroupeById(tableDb, idEleve)
        dataGroupeJson = self.getGroupeFromEleveJoinGroupeById(tableDb, idGroupe)
        
        if (dataEleveJson == []):
            return False
        if (dataGroupeJson != []):
            return False

        if (idEleve == None):
            idEleve = dataEleveJson[0]["Id Eleve"]
        if (idGroupe == None):
            idGroupe = dataGroupeJson[0]["Id Groupe"]

        curs.execute(sql.SQL('UPDATE {}.{} SET eleve_ideleve=%s, groupe_idgroupe=%s WHERE eleve_ideleve=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idEleve, idGroupe, idEleve)
        )
        self.conn.commit()

        return True
