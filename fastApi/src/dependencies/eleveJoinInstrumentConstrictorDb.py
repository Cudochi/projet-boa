#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class EleveJoinInstrumentConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListEleveJoinInstrument(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idEleve = mview[index][rowCount]
            rowCount+=1
            idInstrument = mview[index][rowCount]
            output.append({
            	'Id Eleve': idEleve,
            	'Id Instrument': idInstrument,
            	})

        return output

    def getEleveFromEleveJoinInstrumentById(self, tableDb, idEleve):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT eleve_ideleve, instrument_idinstrument FROM {}.{} WHERE eleve_ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idInstrument = mview[rowCount]
        output.append({
            'Id Eleve': idEleve,
            'Id Instrument': idInstrument,
            })

        return output

    def getInstrumentFromEleveJoinInstrumentById(self, tableDb, idInstrument):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT instrument_idinstrument, eleve_ideleve FROM {}.{} WHERE instrument_idinstrument = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idInstrument),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idInstrument = mview[rowCount]
        output.append({
            'Id Instrument': idInstrument,
            'Id Eleve': idEleve,
            })

        return output

    def getPairFromEleveJoinInstrumentById(self, tableDb, idEleve, idInstrument):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT eleve_ideleve, instrument_idinstrument FROM {}.{} WHERE eleve_ideleve = %s AND instrument_idinstrument = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), str(idInstrument),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idEleve = mview[rowCount]
        rowCount+=1
        idInstrument = mview[rowCount]
        output.append({
            'Id Eleve': idEleve,
            'Id Instrument': idInstrument,
            })

        return output

    def insertEleveJoinInstrument(self, tableDb, idEleve, idInstrument):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(eleve_ideleve, instrument_idinstrument) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idEleve, idInstrument,)
        )

        self.conn.commit()

    def deleteEleveFromEleveJoinInstrument(self, tableDb, idEleve):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE eleve_ideleve = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve),)
        )

        self.conn.commit()

    def deleteInstrumentFromEleveJoinInstrument(self, tableDb, idInstrument):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE instrument_idinstrument = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idInstrument),)
        )

        self.conn.commit()

    def deletePairFromEleveJoinInstrument(self, tableDb, idEleve, idInstrument):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE eleve_ideleve = %s AND instrument_idinstrument = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), str(idInstrument),)
        )

        self.conn.commit()

    def updateEleveJoinInstrument(self, tableDb, idEleve, idInstrument):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataEleveJson = self.getEleveFromEleveJoinInstrumentById(tableDb, idEleve)
        dataInstrumentJson = self.getInstrumentFromEleveJoinInstrumentById(tableDb, idInstrument)
        
        if (dataEleveJson == []):
            return False
        if (dataInstrumentJson != []):
            return False

        if (idEleve == None):
            idEleve = dataEleveJson[0]["Id Eleve"]
        if (idInstrument == None):
            idInstrument = dataInstrumentJson[0]["Id Instrument"]

        curs.execute(sql.SQL('UPDATE {}.{} SET eleve_ideleve=%s, instrument_idinstrument=%s WHERE eleve_ideleve=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idEleve, idInstrument, idEleve)
        )
        self.conn.commit()

        return True
