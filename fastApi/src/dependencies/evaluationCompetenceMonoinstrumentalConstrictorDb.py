#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class EvaluationCompetenceMonoinstrumentalConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListEvaluationCompetenceMonoinstrumental(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idMonoinstrumental = mview[index][rowCount]
            rowCount+=1
            idCompetence = mview[index][rowCount]
            rowCount+=1
            statusCompetence = mview[index][rowCount]
            output.append({
            	'Id Monoinstrumental': idMonoinstrumental,
            	'Id Competence': idCompetence,
                'Status Competence': statusCompetence,
            	})

        return output

    def getEvaluationFromEvaluationCompetenceMonoinstrumentalById(self, tableDb, idMonoinstrumental):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT monoinstrumental_idmonoinstrumental, competence_idcompetence, statuscompetence FROM {}.{} WHERE monoinstrumental_idmonoinstrumental = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idMonoinstrumental),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idMonoinstrumental = mview[rowCount]
        rowCount+=1
        idCompetence = mview[rowCount]
        rowCount+=1
        statusCompetence = mview[rowCount]
        output.append({
            'Id Monoinstrumental': idMonoinstrumental,
            'Id Competence': idCompetence,
            'Status Competence': statusCompetence,
            })

        return output

    # def getProjetFromEvaluationCompetenceMonoinstrumentalById(self, tableDb, idCompetence):
    #     curs = self.cursor
    #
    #     self.checkIfTableExist(tableDb)
    #
    #     curs.execute(sql.SQL('SELECT competence_idcompetence, monoinstrumental_idmonoinstrumental, statuscompetence FROM {}.{} WHERE competence_idcompetence = %s;').format(
    #         sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
    #         ), (str(idCompetence),)
    #     )
    #
    #     mview = curs.fetchone()
    #     
    #     output = []
    #
    #     if (str(type(mview)) == "<class 'NoneType'>"):
    #         return output
    #
    #     rowCount = 0
    #     idMonoinstrumental = mview[rowCount]
    #     rowCount+=1
    #     idCompetence = mview[rowCount]
    #     rowCount+=1
    #     statusCompetence = mview[rowCount]
    #     output.append({
    #         'Id Monoinstrumental': idMonoinstrumental,
    #         'Id Competence': idCompetence,
    #         'Status Competence': statusCompetence,
    #         })
    #
    #     return output

    def getPairFromEvaluationCompetenceMonoinstrumentalById(self, tableDb, idMonoinstrumental, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT monoinstrumental_idmonoinstrumental, competence_idcompetence, statuscompetence FROM {}.{} WHERE monoinstrumental_idmonoinstrumental = %s AND competence_idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idMonoinstrumental), str(idCompetence),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idMonoinstrumental = mview[rowCount]
        rowCount+=1
        idCompetence = mview[rowCount]
        rowCount+=1
        statusCompetence = mview[rowCount]
        output.append({
            'Id Monoinstrumental': idMonoinstrumental,
            'Id Competence': idCompetence,
            'Status Competence': statusCompetence,
            })

        return output

    def insertEvaluationCompetenceMonoinstrumental(self, tableDb, idMonoinstrumental, idCompetence, statusCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(monoinstrumental_idmonoinstrumental, competence_idcompetence, statuscompetence) VALUES (%s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idMonoinstrumental, idCompetence, statusCompetence)
        )

        self.conn.commit()

    def deleteGroupeFromEvaluationCompetenceMonoinstrumental(self, tableDb, idMonoinstrumental):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE monoinstrumental_idmonoinstrumental = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idMonoinstrumental),)
        )

        self.conn.commit()

    def deleteProjetFromEvaluationCompetenceMonoinstrumental(self, tableDb, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE competence_idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCompetence),)
        )

        self.conn.commit()

    def deletePairFromEvaluationCompetenceMonoinstrumental(self, tableDb, idMonoinstrumental, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE monoinstrumental_idmonoinstrumental = %s AND competence_idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idMonoinstrumental), str(idCompetence),)
        )

        self.conn.commit()

    def updateEvaluationCompetenceMonoinstrumental(self, tableDb, idMonoinstrumental, idCompetence, statusCompetence):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('UPDATE {}.{} SET monoinstrumental_idmonoinstrumental=%s, competence_idcompetence=%s, statuscompetence=%s WHERE monoinstrumental_idmonoinstrumental=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idMonoinstrumental, idCompetence, statusCompetence, idMonoinstrumental)
        )
        self.conn.commit()

        return True
