#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class GroupeConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListGroupe(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idGroupe= mview[index][rowCount]
            rowCount+=1
            nomGroupe = mview[index][rowCount]
            rowCount+=1
            collectifOuAtelierGroupe = mview[index][rowCount]
            rowCount+=1
            isVisibleGroupe = mview[index][rowCount]
            output.append({
            	'Id': idGroupe,
            	'Nom': nomGroupe,
            	'Collectif ou Atelier': collectifOuAtelierGroupe,
            	'Est Visible': isVisibleGroupe
            	})

        return output

    def getGroupeById(self, tableDb, idGroupe):
        if not isinstance(idGroupe, int):
            return []

        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT idgroupe, nomgroupe, collectifouateliergroupe, visiblegroupe FROM {}.{} WHERE idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        mview = curs.fetchone()

        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idGroupe = mview[rowCount]
        rowCount+=1
        nomGroupe = mview[rowCount]
        rowCount+=1
        collectifOuAtelierGroupe = mview[rowCount]
        rowCount+=1
        isVisibleGroupe = mview[rowCount]
        output.append({
            'Id': idGroupe,
            'Nom': nomGroupe,
            'Collectif ou Atelier': collectifOuAtelierGroupe,
            'Est Visible': isVisibleGroupe
            })

        return output

    def insertGroupe(self, tableDb, nomGroupe, collectifOuAtelierGroupe, isVisibleGroupe):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(nomgroupe, collectifouateliergroupe, visiblegroupe) VALUES (%s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomGroupe, str(collectifOuAtelierGroupe), isVisibleGroupe)
        )

        self.conn.commit()

    def deleteGroupe(self, tableDb, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        self.conn.commit()

    def updateGroupe(self, tableDb, idGroupe, nomGroupe, collectifOuAtelierGroupe, isVisibleGroupe):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getGroupeById(tableDb, idGroupe)
        
        if (dataJson == []):
            return False

        if (nomGroupe == None):
            nomGroupe = dataJson[0]["Nom"]
        if (collectifOuAtelierGroupe == None):
            collectifOuAtelierGroupe = dataJson[0]["Collectif ou Atelier"]
        if (isVisibleGroupe == None): 
            isVisibleGroupe = dataJson[0]["Est Visible"]

        curs.execute(sql.SQL('UPDATE {}.{} SET nomgroupe=%s, collectifouateliergroupe=%s, visiblegroupe=%s WHERE idgroupe=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomGroupe, collectifOuAtelierGroupe, isVisibleGroupe, idGroupe)
        )
        self.conn.commit()

        return True
