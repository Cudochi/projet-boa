#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class GroupeJoinProjetConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListGroupeJoinProjet(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idGroupe = mview[index][rowCount]
            rowCount+=1
            idProjet = mview[index][rowCount]
            output.append({
            	'Id Groupe': idGroupe,
            	'Id Projet': idProjet,
            	})

        return output

    def getGroupeFromGroupeJoinProjetById(self, tableDb, idGroupe):
        if not isinstance(idGroupe, int):
            return []
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT groupe_idgroupe, projet_idprojet FROM {}.{} WHERE groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idGroupe = mview[rowCount]
        rowCount+=1
        idProjet = mview[rowCount]
        output.append({
            'Id Groupe': idGroupe,
            'Id Projet': idProjet,
            })

        return output

    def getProjetFromGroupeJoinProjetById(self, tableDb, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT projet_idprojet, groupe_idgroupe FROM {}.{} WHERE projet_idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idGroupe = mview[rowCount]
        rowCount+=1
        idProjet = mview[rowCount]
        output.append({
            'Id Projet': idProjet,
            'Id Groupe': idGroupe,
            })

        return output

    def getPairFromGroupeJoinProjetById(self, tableDb, idGroupe, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT groupe_idgroupe, projet_idprojet FROM {}.{} WHERE groupe_idgroupe = %s AND projet_idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe), str(idProjet),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idGroupe = mview[rowCount]
        rowCount+=1
        idProjet = mview[rowCount]
        output.append({
            'Id Groupe': idGroupe,
            'Id Projet': idProjet,
            })

        return output

    def insertGroupeJoinProjet(self, tableDb, idGroupe, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(groupe_idgroupe, projet_idprojet) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idGroupe, idProjet,)
        )

        self.conn.commit()

    def deleteGroupeFromGroupeJoinProjet(self, tableDb, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        self.conn.commit()

    def deleteProjetFromGroupeJoinProjet(self, tableDb, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE projet_idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),)
        )

        self.conn.commit()

    def deletePairFromGroupeJoinProjet(self, tableDb, idGroupe, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE groupe_idgroupe = %s AND projet_idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe), str(idProjet),)
        )

        self.conn.commit()

    def updateGroupeJoinProjet(self, tableDb, idGroupe, idProjet):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataGroupeJson = self.getGroupeFromGroupeJoinProjetById(tableDb, idGroupe)
        dataProjetJson = self.getProjetFromGroupeJoinProjetById(tableDb, idProjet)
        
        if (dataGroupeJson == []):
            return False
        if (dataProjetJson != []):
            return False

        if (idGroupe == None):
            idGroupe = dataGroupeJson[0]["Id Groupe"]
        if (idProjet == None):
            idProjet = dataProjetJson[0]["Id Projet"]

        curs.execute(sql.SQL('UPDATE {}.{} SET groupe_idgroupe=%s, projet_idprojet=%s WHERE groupe_idgroupe=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idGroupe, idProjet, idGroupe)
        )
        self.conn.commit()

        return True
