#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class InstrumentConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListInstrument(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idInstrument = mview[index][rowCount]
            rowCount+=1
            nomInstrument = mview[index][rowCount]
            rowCount+=1
            isVisibleInstrument = mview[index][rowCount]
            output.append({
            	'Id': idInstrument,
            	'Nom': nomInstrument,
            	'Est Visible': isVisibleInstrument
            	})

        return output

    def getInstrumentById(self, tableDb, idInstrument):
        if not isinstance(idInstrument, int):
            return []
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT idinstrument, nominstrument, visibleinstrument FROM {}.{} WHERE idinstrument = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idInstrument),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idInstrument = mview[rowCount]
        rowCount+=1
        nomInstrument = mview[rowCount]
        rowCount+=1
        isVisibleInstrument = mview[rowCount]
        output.append({
            'Id': idInstrument,
            'Nom': nomInstrument,
            'Est Visible': isVisibleInstrument
            })

        return output

    def insertInstrument(self, tableDb, nomInstrument, isVisibleInstrument):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(nominstrument, visibleinstrument) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomInstrument, isVisibleInstrument)
        )

        self.conn.commit()

    def deleteInstrument(self, tableDb, idInstrument):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idinstrument = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idInstrument),)
        )

        self.conn.commit()

    def updateInstrument(self, tableDb, idInstrument, nomInstrument, isVisibleInstrument):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getInstrumentById(tableDb, idInstrument)
        
        if (dataJson == []):
            return False

        if (nomInstrument == None):
            nomInstrument = dataJson[0]["Nom"]
        if (isVisibleInstrument == None): 
            isVisibleInstrument = dataJson[0]["Est Visible"]

        curs.execute(sql.SQL('UPDATE {}.{} SET nominstrument=%s, visibleinstrument=%s WHERE idinstrument=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomInstrument, isVisibleInstrument, idInstrument)
        )
        self.conn.commit()

        return True
