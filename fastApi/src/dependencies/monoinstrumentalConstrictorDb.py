#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class MonoinstrumentalConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListMonoinstrumental(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idMonoinstrumental = mview[index][rowCount]
            rowCount+=1
            idEleve = mview[index][rowCount]
            rowCount+=1
            idProfesseur = mview[index][rowCount]
            rowCount+=1
            bilanMonoinstrumental = mview[index][rowCount]
            rowCount+=1
            dateBilanMonoinstrumental = mview[index][rowCount]
            output.append({
            	'Id Bilan Eleve Musique Collective': idMonoinstrumental,
            	'Id Eleve': idEleve,
            	'Id Professeur': idProfesseur,
            	'Bilan': bilanMonoinstrumental,
                'Date Bilan': dateBilanMonoinstrumental
            	})

        return output

    def getMonoinstrumentalById(self, tableDb, idMonoinstrumental):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT * FROM {}.{} WHERE idmonoinstrumental = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idMonoinstrumental),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idMonoinstrumental = mview[rowCount]
        rowCount+=1
        idEleve = mview[rowCount]
        rowCount+=1
        idProfesseur = mview[rowCount]
        rowCount+=1
        bilanMonoinstrumental = mview[rowCount]
        rowCount+=1
        dateBilanMonoinstrumental = mview[rowCount]
        output.append({
            'Id Bilan Eleve Musique Collective': idMonoinstrumental,
            'Id Eleve': idEleve,
            'Id Professeur': idProfesseur,
            'Bilan': bilanMonoinstrumental,
            'Date Bilan': dateBilanMonoinstrumental
            })

        return output

    def insertMonoinstrumental(self, 
            tableDb, 
            idEleve, 
            idProfesseur, 
            bilanMonoinstrumental, 
            dateBilanMonoinstrumental
            ):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(ideleve, idprofesseur, bilanmonoinstrumental, datebilanmonoinstrumental) VALUES (%s, %s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idEleve), 
                str(idProfesseur), 
                bilanMonoinstrumental, 
                dateBilanMonoinstrumental
                )
        )

        self.conn.commit()

    def deleteMonoinstrumental(self, tableDb, idMonoinstrumental):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idmonoinstrumental = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idMonoinstrumental),)
        )

        self.conn.commit()

    def updateMonoinstrumental(self, 
            tableDb, 
            idMonoinstrumental,
            idEleve, 
            idProfesseur, 
            bilanMonoinstrumental, 
            dateBilanMonoinstrumental
            ):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getMonoinstrumentalById(tableDb, idMonoinstrumental)
        
        if (dataJson == []):
            return False

        if (idMonoinstrumental == None):
            idMonoinstrumental = dataJson[0]["Id Bilan Musique Collective"]
        if (idEleve == None):
            idEleve = dataJson[0]["Id Eleve"]
        if (idProfesseur == None):
            idProfesseur = dataJson[0]["Id Professeur"]
        if (bilanMonoinstrumental == None):
            bilanMonoinstrumental = dataJson[0]["Bilan"]
        if (dateBilanMonoinstrumental == None):
            dateBilanMonoinstrumental = dataJson[0]["Date Bilan"]

        curs.execute(sql.SQL('UPDATE {}.{} SET idmonoinstrumental=%s, ideleve=%s, idprofesseur=%s, bilanmonoinstrumental=%s, datebilanmonoinstrumental=%s WHERE idmonoinstrumental=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idMonoinstrumental),
                str(idEleve), 
                str(idProfesseur), 
                bilanMonoinstrumental, 
                dateBilanMonoinstrumental,
                idMonoinstrumental
                )
            )

        self.conn.commit()

        return True
