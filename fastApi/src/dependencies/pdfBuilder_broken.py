#!/usr/bin/env python
# -*- coding: utf-8 -*-
import gi
import sys
import os
import shutil
import importlib
import pdfkit
import sqlite3
import in_place
from fpdf import FPDF
from pprint import pprint
from datetime import datetime
from PyPDF2 import PdfFileReader, PdfFileMerger
from jinja2 import Environment, FileSystemLoader

importlib.reload(sys)
#sys.setdefaultencoding("utf-8")
# iso-8859-1

# Indication de la version de GTK+ 3 utilise et import de GTK
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

# Fonction qui permet de standardiser les chemins d'acces aux ressources pour rendre le programme compatible windows & linux
def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    
    return os.path.join(base_path, relative_path)

# Genere le fichier PDF a partir du model en HTML
def buildPDFFileStudent(
        builder,
        resource_path):
    # Cree une copie temporaire du model pour le modifier
    shutil.copy2("templates/bilan_eleve_pour_html_v2.html","templates/bilan_eleve_pour_html_v2_temp.html")

    # Se connecte a un Widget de l'interface
    GtkEntryStudentFirstName = builder.get_object("GtkEntryStudentFirstName")
    # En recupere le texte
    StudentFirstName = GtkEntryStudentFirstName.get_text()
    # Se connecte a un Widget de l'interface
    GtkEntryStudentLastName = builder.get_object("GtkEntryStudentLastName")
    # En recupere le texte
    StudentLastName = GtkEntryStudentLastName.get_text()
    # Se connecte a un Widget de l'interface
    TextView2ProjectSummary = builder.get_object("TextView2ProjectSummary")
    window = builder.get_object("window1")
    window.textbuffer = TextView2ProjectSummary.get_buffer()
    # En recupere le texte
    GtkTextViewStdSth_start_iter = window.textbuffer.get_start_iter()
    GtkTextViewStdSth_end_iter = window.textbuffer.get_end_iter()
    StudentSynthesis = window.textbuffer.get_text(GtkTextViewStdSth_start_iter, GtkTextViewStdSth_end_iter, True)

    # Inscrit les donnees dans le fichier HTML temporaire
    with in_place.InPlace("templates/bilan_eleve_pour_html_v2_temp.html") as file:
        for line in file:
            line = line.replace("first_name", StudentFirstName)
            line = line.replace("last_name", StudentLastName)
            line = line.replace("synthesis_line_1", StudentSynthesis)
            file.write(line)

    pdfkit.from_file(resource_path("templates/bilan_eleve_pour_html_v2_temp.html"), 'reports/bilan_eleve.pdf')
    print("PDF bilan_eleve.pdf generated !")
    os.remove("templates/bilan_eleve_pour_html_v2_temp.html")

def buildPDFFileStudent2(
        label2FirstNameText,
        label2LastNameText,
        label2InstrumentText,
        label2CycleText,
        label2GroupText,
        label2YearText,
        Entry2ProfessorText,
        StudentSynthesis,
        EvaluationList,
        AutoEvaluationList,
        label2ProjectText,
        anneeScolaireCourante):

    # Cree une copie temporaire du model pour le modifier
    shutil.copy2("templates/bilan_eleve_pour_html_v2.html","templates/bilan_eleve_pour_html_v2_temp.html")

    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("templates/bilan_eleve_pour_html_v2.html")
    file = open("templates/bilan_eleve_pour_html_v2_temp.html", "wb")

    template_vars = {"school_year": anneeScolaireCourante,
                    "last_name": label2LastNameText,
                    "first_name": label2FirstNameText,
                    "collectif_name": label2GroupText,
                    "cycle": label2CycleText,
                    "year": label2YearText,
                    "professor": Entry2ProfessorText,
                    "report_date": str(datetime.now().day) + "/" + str(datetime.now().month) + "/" + str(datetime.now().year),
                    "report": StudentSynthesis}
    html_out = template.render(template_vars).encode("utf-8")
    file.write(html_out)
    file.close()

    pdfName = "bilan_" + label2FirstNameText + "_" + label2LastNameText + "_" + Entry2ProfessorText + "_" + label2YearText
    pdfkit.from_file(resource_path("templates/bilan_eleve_pour_html_v2_temp.html"), "reports/" + pdfName)
    print("PDF bilan_eleve.pdf generated !")
    os.remove("templates/bilan_eleve_pour_html_v2_temp.html")
    #-------------------------------------------------------------------------------------------
    shutil.copy2("templates/bilan_competence_eleve.html","templates/bilan_competence_eleve_temp.html")

    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("templates/bilan_competence_eleve.html")
    file = open("templates/bilan_competence_eleve_temp.html", "wb")

    template_vars = {"school_year": anneeScolaireCourante,
                    "eleveLastName": label2LastNameText,
                    "eleveFirstName": label2FirstNameText,
                    "collectif_name": label2GroupText,
                    "cycle": label2CycleText,
                    "year": label2YearText,
                    "professor": Entry2ProfessorText,
                    "reportDate": str(datetime.now().day) + "/" + str(datetime.now().month) + "/" + str(datetime.now().year)}

    for lineIndex in range(0,competenciesNumberStudent):
        for columnIndex in range(1,5):
            if(EvaluationList[lineIndex-1] == columnIndex):
                template_vars["a"+str(lineIndex) + str(columnIndex)] = "X"
            else:
                template_vars["a"+str(lineIndex) + str(columnIndex)] = ""
        for columnIndex in range(5,9):
            if(AutoEvaluationList[lineIndex-1] == columnIndex-4):
                template_vars["a"+str(lineIndex) + str(columnIndex)] = "X"
            else:
                template_vars["a"+str(lineIndex) + str(columnIndex)] = ""

    html_out = template.render(template_vars).encode( "utf-8" )

    file.write(html_out)
    file.close()

    pdfNameBilanCompetenceEleve = label2LastNameText + "-" + label2FirstNameText + "-" + "bilan_competence" + "-" + ANNEE_SCOLAIRE_COURANTE + ".pdf"
    pdfkit.from_file(resource_path("templates/bilan_competence_eleve_temp.html"), "reports/" + pdfNameBilanCompetenceEleve)
    print("PDF bilan_competence_eleve.pdf generated !")
    os.remove("templates/bilan_competence_eleve_temp.html")

    pdf_file1 = PdfFileReader("reports/" + pdfName)
    pdf_file2 = PdfFileReader("reports/" + pdfNameBilanCompetenceEleve)
    output = PdfFileMerger()
    output.append(pdf_file1, import_bookmarks=False)
    output.append(pdf_file2, import_bookmarks=False)
    pdfName = label2LastNameText + "-" + label2FirstNameText + "-" + label2ProjectText + "-" + ANNEE_SCOLAIRE_COURANTE + ".pdf"
    print(pdfName)
    output.write("reports/" + pdfName)

