import gi
import sys
import os
import shutil
import importlib
import pdfkit
# import in_place
# from fpdf import FPDF
# from pprint import pprint
from datetime import datetime
from PyPDF2 import PdfFileReader, PdfFileMerger
from jinja2 import Environment, FileSystemLoader

from pdfBuilder_broken import resource_path, buildPDFFileStudent2

importlib.reload(sys)

global competenciesNumberStudent
# Variable globale du nombre de compétences dans la page Eleve
competenciesNumberStudent = 33

global competenciesNumberProject
# Variable globale du nombre de compétences dans la page Projet
competenciesNumberProject = 33

ANNEE_SCOLAIRE_COURANTE = "2023-2024"

def buildPDFFileStudentAnnualProject(
        label2FirstNameText,
        label2LastNameText,
        label2InstrumentText,
        label2CycleText,
        label2GroupText,
        label2YearText,
        Entry2ProfessorText,
        AnnualReport,
        EvaluationList,
        AutoEvaluationList,
        label2ReportDateText):

    # Cree une copie temporaire du model pour le modifier
    shutil.copy2("templates/bilan_eleve_pour_html_v2.html","templates/bilan_eleve_pour_html_v2_temp.html")

    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("templates/bilan_eleve_pour_html_v2.html")
    file = open("templates/bilan_eleve_pour_html_v2_temp.html", "wb")

    if(label2ReportDateText == "" or label2ReportDateText == "aaaa-mm-jj"):
        label2ReportDateText = str(datetime.now().day) + "/" + str(datetime.now().month) + "/" + str(datetime.now().year)

    template_vars = {"school_year": ANNEE_SCOLAIRE_COURANTE,
                    "last_name": label2LastNameText,
                    "first_name": label2FirstNameText,
                    "collectif_name": label2GroupText,
                    "cycle": label2CycleText,
                    "year": label2YearText,
                    "professor": Entry2ProfessorText,
                    "report_date": label2ReportDateText,
                    "report": AnnualReport}
    html_out = template.render(template_vars).encode("utf-8")
    print(type(html_out))
    file.write(html_out)
    file.close()

    pdfName = "bilan_collectif_annuel_" + label2FirstNameText + "_" + label2LastNameText + "_" + Entry2ProfessorText + "_" + label2YearText
    options = {"enable-local-file-access": None}
    pdfkit.from_file(resource_path("templates/bilan_eleve_pour_html_v2_temp.html"), "reports/" + pdfName, options=options)
    print("PDF bilan_eleve.pdf generated !")
    os.remove("templates/bilan_eleve_pour_html_v2_temp.html")
    #-------------------------------------------------------------------------------------------
    shutil.copy2("templates/bilan_competence_eleve.html","templates/bilan_competence_eleve_temp.html")

    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("templates/bilan_competence_eleve.html")
    file = open("templates/bilan_competence_eleve_temp.html", "wb")

    template_vars = {"school_year": ANNEE_SCOLAIRE_COURANTE,
                    "eleveLastName": label2LastNameText,
                    "eleveFirstName": label2FirstNameText,
                    "collectif_name": label2GroupText,
                    "cycle": label2CycleText,
                    "year": label2YearText,
                    "professor": Entry2ProfessorText,
                    "reportDate": str(datetime.now().day) + "/" + str(datetime.now().month) + "/" + str(datetime.now().year)}

    # Commenté pour les tests
    # for lineIndex in range(0,competenciesNumberStudent):
    #     for columnIndex in range(1,5):
    #         if(EvaluationList[lineIndex-1] == columnIndex):
    #             template_vars["a"+str(lineIndex) + str(columnIndex)] = "X"
    #         else:
    #             template_vars["a"+str(lineIndex) + str(columnIndex)] = ""
    #     for columnIndex in range(5,9):
    #         if(AutoEvaluationList[lineIndex-1] == columnIndex-4):
    #             template_vars["a"+str(lineIndex) + str(columnIndex)] = "X"
    #         else:
    #             template_vars["a"+str(lineIndex) + str(columnIndex)] = ""

    html_out = template.render(template_vars).encode( "utf-8" )

    file.write(html_out)
    file.close()

    pdfNameBilanCompetenceEleve = label2LastNameText + "-" + label2FirstNameText + "-" + "bilan_collectif_annuel" + "-" + ANNEE_SCOLAIRE_COURANTE + ".pdf"
    pdfkit.from_file(resource_path("templates/bilan_competence_eleve_temp.html"), "reports/" + pdfNameBilanCompetenceEleve)
    print("PDF bilan_competence_eleve.pdf generated !")
    os.remove("templates/bilan_competence_eleve_temp.html")

    pdf_file1 = PdfFileReader("reports/" + pdfName)
    pdf_file2 = PdfFileReader("reports/" + pdfNameBilanCompetenceEleve)
    output = PdfFileMerge()
    output.append(pdf_file1, import_bookmarks=False)
    output.append(pdf_file2, import_bookmarks=False)
    pdfName = label2LastNameText + "-" + label2FirstNameText + "-" + "bilan_collectif_annuel" + "-" + ANNEE_SCOLAIRE_COURANTE + ".pdf"
    print(pdfName)
    output.write("reports/" + pdfName)

def buildPDFFileProject(
        self,
        Entry3ProjectNameText,
        Entry3ProfessorsText,
        ComboBoxText3GroupText,
        Entry3SchoolYearText,
        Entry3DateBeginText,
        Entry3DateEndText,
        TextView3PresentationProjetText,
        TextView3BilanProjetText):

    # Cree une copie temporaire du model pour le modifier
    shutil.copy2("templates/bilan_projet.html","templates/bilan_projet_temp.html")

    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("templates/bilan_projet.html")
    file = open("templates/bilan_projet_temp.html", "wb")

    SynthesisSkillList = ScanCheckButtonCollection(self, 41, 0)
    AimedSkillList = ScanCheckButtonCollection(self, 41, 1)

    template_vars = {"project_name": Entry3ProjectNameText,
                    "project_teachers": Entry3ProfessorsText,
                    "group_name": ComboBoxText3GroupText,
                    "dd": Entry3DateBeginText,
                    "df": Entry3DateEndText,
                    "project_presentation_text": TextView3PresentationProjetText,
                    "project_report_text": TextView3BilanProjetText}

    for lineIndex in range(1,41):
        if(SynthesisSkillList[lineIndex-1] == True):
            template_vars["a" + str(1) + str(lineIndex)] = "X"
        else:
            template_vars["a" + str(10) + str(lineIndex)] = ""
        if(AimedSkillList[lineIndex-1] == True):
            template_vars["b" + str(1) + str(lineIndex)] = "X"
        else:
            template_vars["a" + str(100) + str(lineIndex)] = ""

    html_out = template.render(template_vars).encode("utf-8 ")
    file.write(html_out)
    file.close()

    SynthesisProjectFile = str(ANNEE_SCOLAIRE_COURANTE) + "_" + Entry3ProjectNameText + "_" + ComboBoxText3GroupText + ".pdf"
    pdfkit.from_file(resource_path("templates/bilan_projet_temp.html"), "reports/" + SynthesisProjectFile)
    print("PDF bilan_projet.pdf generated !")
    os.remove("templates/bilan_projet_temp.html")

buildPDFFileStudentAnnualProject(
        "Kavan",
        "LEHO",
        "Guitare",
        "TEST",
        "Queen",
        "TEST",
        "Yann",
        "TEST TEST TEST",
        "",
        "",
        "TEST")
