#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class ProfesseurConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListProfesseur(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idProfesseur= mview[index][rowCount]
            rowCount+=1
            nomProfesseur = mview[index][rowCount]
            rowCount+=1
            prenomProfesseur = mview[index][rowCount]
            rowCount+=1
            isVisibleProfesseur = mview[index][rowCount]
            output.append({
            	'Id': idProfesseur,
            	'Nom': nomProfesseur,
            	'Prenom': prenomProfesseur,
            	'Est Visible': isVisibleProfesseur
            	})

        return output

    def getProfesseurById(self, tableDb, idProfesseur):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT idprofesseur, nomprofesseur, prenomprofesseur, visibleprofesseur FROM {}.{} WHERE idprofesseur = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProfesseur),)
        )

        mview = curs.fetchone()

        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProfesseur = mview[rowCount]
        rowCount+=1
        nomProfesseur = mview[rowCount]
        rowCount+=1
        prenomProfesseur = mview[rowCount]
        rowCount+=1
        isVisibleProfesseur = mview[rowCount]
        output.append({
            'Id': idProfesseur,
            'Nom': nomProfesseur,
            'Prenom': prenomProfesseur,
            'Est Visible': isVisibleProfesseur
            })

        return output

    def insertProfesseur(self, tableDb, nomProfesseur, prenomProfesseur, isVisibleProfesseur):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(nomprofesseur, prenomprofesseur, visibleprofesseur) VALUES (%s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomProfesseur, prenomProfesseur, isVisibleProfesseur)
        )

        self.conn.commit()

    def deleteProfesseur(self, tableDb, idProfesseur):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idprofesseur = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProfesseur),)
        )

        self.conn.commit()

    def updateProfesseur(self, tableDb, idProfesseur, nomProfesseur, prenomProfesseur, isVisibleProfesseur):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getProfesseurById(tableDb, idProfesseur)
        
        if (dataJson == []):
            return False

        if (nomProfesseur == None):
            nomProfesseur = dataJson[0]["Nom"]
        if (prenomProfesseur == None):
            prenomProfesseur = dataJson[0]["Prenom"]
        if (isVisibleProfesseur == None): 
            isVisibleProfesseur = dataJson[0]["Est Visible"]

        curs.execute(sql.SQL('UPDATE {}.{} SET nomprofesseur=%s, prenomprofesseur=%s, visibleprofesseur=%s WHERE idprofesseur=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomProfesseur, prenomProfesseur, isVisibleProfesseur, idProfesseur)
        )
        self.conn.commit()

        return True
