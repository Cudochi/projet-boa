#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class ProfesseurJoinGroupeConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListProfesseurJoinGroupe(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idProfesseur = mview[index][rowCount]
            rowCount+=1
            idGroupe = mview[index][rowCount]
            output.append({
            	'Id Professeur': idProfesseur,
            	'Id Groupe': idGroupe,
            	})

        return output

    def getProfesseurFromProfesseurJoinGroupeById(self, tableDb, idProfesseur):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT professeur_idprofesseur, groupe_idgroupe FROM {}.{} WHERE professeur_idprofesseur = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProfesseur),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProfesseur = mview[rowCount]
        rowCount+=1
        idGroupe = mview[rowCount]
        output.append({
            'Id Professeur': idProfesseur,
            'Id Groupe': idGroupe,
            })

        return output

    def getGroupeFromProfesseurJoinGroupeById(self, tableDb, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT groupe_idgroupe, professeur_idprofesseur FROM {}.{} WHERE groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProfesseur = mview[rowCount]
        rowCount+=1
        idGroupe = mview[rowCount]
        output.append({
            'Id Groupe': idGroupe,
            'Id Professeur': idProfesseur,
            })

        return output

    def getPairFromProfesseurJoinGroupeById(self, tableDb, idProfesseur, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT professeur_idprofesseur, groupe_idgroupe FROM {}.{} WHERE professeur_idprofesseur = %s AND groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProfesseur), str(idGroupe),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProfesseur = mview[rowCount]
        rowCount+=1
        idGroupe = mview[rowCount]
        output.append({
            'Id Professeur': idProfesseur,
            'Id Groupe': idGroupe,
            })

        return output

    def insertProfesseurJoinGroupe(self, tableDb, idProfesseur, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(professeur_idprofesseur, groupe_idgroupe) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idProfesseur, idGroupe,)
        )

        self.conn.commit()

    def deleteProfesseurFromProfesseurJoinGroupe(self, tableDb, idProfesseur):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE professeur_idprofesseur = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProfesseur),)
        )

        self.conn.commit()

    def deleteGroupeFromProfesseurJoinGroupe(self, tableDb, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idGroupe),)
        )

        self.conn.commit()

    def deletePairFromProfesseurJoinGroupe(self, tableDb, idProfesseur, idGroupe):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE professeur_idprofesseur = %s AND groupe_idgroupe = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProfesseur), str(idGroupe),)
        )

        self.conn.commit()

    def updateProfesseurJoinGroupe(self, tableDb, idProfesseur, idGroupe):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataProfesseurJson = self.getProfesseurFromProfesseurJoinGroupeById(tableDb, idProfesseur)
        dataGroupeJson = self.getGroupeFromProfesseurJoinGroupeById(tableDb, idGroupe)
        
        if (dataProfesseurJson == []):
            return False
        if (dataGroupeJson != []):
            return False

        if (idProfesseur == None):
            idProfesseur = dataProfesseurJson[0]["Id Professeur"]
        if (idGroupe == None):
            idGroupe = dataGroupeJson[0]["Id Groupe"]

        curs.execute(sql.SQL('UPDATE {}.{} SET professeur_idprofesseur=%s, groupe_idgroupe=%s WHERE professeur_idprofesseur=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idProfesseur, idGroupe, idProfesseur)
        )
        self.conn.commit()

        return True
