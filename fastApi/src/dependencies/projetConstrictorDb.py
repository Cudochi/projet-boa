#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class ProjetConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListProjet(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idProjet = mview[index][rowCount]
            rowCount+=1
            nomProjet = mview[index][rowCount]
            rowCount+=1
            dateDebutProjet = mview[index][rowCount]
            rowCount+=1
            dateFinProjet = mview[index][rowCount]
            rowCount+=1
            presentationProjet = mview[index][rowCount]
            rowCount+=1
            bilanProjet = mview[index][rowCount]
            rowCount+=1
            visibleProjet = mview[index][rowCount]
            output.append({
                'Id Projet': idProjet,
                'Nom Projet': nomProjet,
                'Date Debut Projet': dateDebutProjet,
                'Date Fin Projet': dateFinProjet,
                'Presentation Projet': presentationProjet,
                'Bilan Projet': bilanProjet,
                'Visible Projet': visibleProjet
                })

        return output

    def getProjetById(self, tableDb, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT * FROM {}.{} WHERE idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProjet = mview[rowCount]
        rowCount+=1
        nomProjet = mview[rowCount]
        rowCount+=1
        dateDebutProjet = mview[rowCount]
        rowCount+=1
        dateFinProjet = mview[rowCount]
        rowCount+=1
        presentationProjet = mview[rowCount]
        rowCount+=1
        bilanProjet = mview[rowCount]
        rowCount+=1
        visibleProjet = mview[rowCount]
        output.append({
            'Id Projet': idProjet,
            'Nom Projet': nomProjet,
            'Date Debut Projet': dateDebutProjet,
            'Date Fin Projet': dateFinProjet,
            'Presentation Projet': presentationProjet,
            'Bilan Projet': bilanProjet,
            'Visible Projet': visibleProjet
            })

        return output

    def insertProjet(self, 
            tableDb, 
            nomProjet, 
            dateDebutProjet, 
            dateFinProjet, 
            presentationProjet, 
            bilanProjet, 
            visibleProjet 
            ):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('INSERT INTO {}.{}(nomprojet, datedebutprojet, datefinprojet, presentationprojet, bilanprojet, visibleprojet) VALUES (%s, %s, %s, %s, %s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (nomProjet, 
                dateDebutProjet, 
                dateFinProjet, 
                presentationProjet, 
                bilanProjet,
                str(visibleProjet)
                )
        )

        self.conn.commit()

    def deleteProjet(self, tableDb, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),)
        )

        self.conn.commit()

    def updateProjet(self, 
            tableDb, 
            idProjet,
            nomProjet, 
            dateDebutProjet, 
            dateFinProjet, 
            presentationProjet, 
            bilanProjet, 
            visibleProjet 
            ):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataJson = self.getProjetById(tableDb, idProjet)
        
        if (dataJson == []):
            return False

        if (idProjet == None):
            idProjet = dataJson[0]["Id Projet"]
        if (nomProjet == None):
            nomProjet = dataJson[0]["Nom Projet"]
        if (dateDebutProjet == None):
            dateDebutProjet = dataJson[0]["Date Debut Projet"]
        if (dateFinProjet == None):
            dateFinProjet = dataJson[0]["Date Fin Projet"]
        if (presentationProjet == None):
            presentationProjet = dataJson[0]["Presentation Projet"]
        if (bilanProjet == None):
            bilanProjet = dataJson[0]["Bilan Projet"]
        if (visibleProjet == None):
            visibleProjet = dataJson[0]["Visible Projet"]

        curs.execute(sql.SQL('UPDATE {}.{} SET idprojet=%s, nomprojet=%s, datedebutprojet=%s, datefinprojet=%s, presentationprojet=%s, bilanprojet=%s, visibleprojet=%s WHERE idprojet=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),
                nomProjet, 
                dateDebutProjet, 
                dateFinProjet, 
                presentationProjet, 
                bilanProjet,
                str(visibleProjet),
                str(idProjet)
                )
            )

        self.conn.commit()

        return True
