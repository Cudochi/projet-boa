#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import HTTPException
from starlette.status import HTTP_404_NOT_FOUND
from src.dependencies.constrictorDb import ConstrictorDb 
from src.config.envVars import envVarsGetter

class ProjetJoinCompetenceConstrictor(ConstrictorDb):

    def __init__(self):
        super().__init__()

    def getListProjetJoinCompetence(self, tableDb):
        curs = self.cursor

        self.checkIfTableExist(tableDb)
        
        curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            )
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idProjet = mview[index][rowCount]
            rowCount+=1
            idCompetence = mview[index][rowCount]
            output.append({
            	'Id Projet': idProjet,
            	'Id Competence': idCompetence,
            	})

        return output

    def getCompetenceListFromProjetJoinCompetenceById(self, tableDb, idProjet):
        if not isinstance(idProjet, int):
            return []
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT projet_idprojet, competence_idcompetence FROM {}.{} WHERE projet_idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),)
        )

        mview = curs.fetchall()

        output = []
        for index in range(0,len(mview)):
            rowCount = 0
            idProjet = mview[index][rowCount]
            rowCount+=1
            idCompetence = mview[index][rowCount]
            output.append({
            	'Id Projet': idProjet,
            	'Id Competence': idCompetence,
            	})

        return output

    def getProjetFromProjetJoinCompetenceById(self, tableDb, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT projet_idprojet, competence_idcompetence FROM {}.{} WHERE projet_idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProjet = mview[rowCount]
        rowCount+=1
        idCompetence = mview[rowCount]
        output.append({
            'Id Projet': idProjet,
            'Id Competence': idCompetence,
            })

        return output

    def getCompetenceFromProjetJoinCompetenceById(self, tableDb, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT competence_idcompetence, projet_idprojet FROM {}.{} WHERE competence_idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCompetence),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProjet = mview[rowCount]
        rowCount+=1
        idCompetence = mview[rowCount]
        output.append({
            'Id Competence': idCompetence,
            'Id Projet': idProjet,
            })

        return output

    def getPairFromProjetJoinCompetenceById(self, tableDb, idProjet, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('SELECT projet_idprojet, competence_idcompetence FROM {}.{} WHERE projet_idprojet = %s AND competence_idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet), str(idCompetence),)
        )

        mview = curs.fetchone()
        
        output = []

        if (str(type(mview)) == "<class 'NoneType'>"):
            return output

        rowCount = 0
        idProjet = mview[rowCount]
        rowCount+=1
        idCompetence = mview[rowCount]
        output.append({
            'Id Projet': idProjet,
            'Id Competence': idCompetence,
            })

        return output

    def insertProjetJoinCompetence(self, tableDb, idProjet, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('INSERT INTO {}.{}(projet_idprojet, competence_idcompetence) VALUES (%s, %s);').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idProjet, idCompetence,)
        )

        self.conn.commit()

    def deleteProjetFromProjetJoinCompetence(self, tableDb, idProjet):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE projet_idprojet = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet),)
        )

        self.conn.commit()

    def deleteCompetenceFromProjetJoinCompetence(self, tableDb, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE competence_idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idCompetence),)
        )

        self.conn.commit()

    def deletePairFromProjetJoinCompetence(self, tableDb, idProjet, idCompetence):
        curs = self.cursor

        self.checkIfTableExist(tableDb)

        curs.execute(sql.SQL('DELETE FROM {}.{} WHERE projet_idprojet = %s AND competence_idcompetence = %s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (str(idProjet), str(idCompetence),)
        )

        self.conn.commit()

    def updateProjetJoinCompetence(self, tableDb, idProjet, idCompetence):
        curs = self.cursor
        
        self.checkIfTableExist(tableDb)

        dataProjetJson = self.getProjetFromProjetJoinCompetenceById(tableDb, idProjet)
        dataCompetenceJson = self.getCompetenceFromProjetJoinCompetenceById(tableDb, idCompetence)
        
        if (dataProjetJson == []):
            return False
        if (dataCompetenceJson != []):
            return False

        if (idProjet == None):
            idProjet = dataProjetJson[0]["Id Projet"]
        if (idCompetence == None):
            idCompetence = dataCompetenceJson[0]["Id Competence"]

        curs.execute(sql.SQL('UPDATE {}.{} SET projet_idprojet=%s, competence_idcompetence=%s WHERE projet_idprojet=%s;').format(
            sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDb)
            ), (idProjet, idCompetence, idProjet)
        )
        self.conn.commit()

        return True
