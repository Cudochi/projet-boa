#!/usr/bin/env python3

import psycopg2
from src.config.envVars import envVarsGetter

class PsqlConnector:
    """This class's purpose is to establish a connection with the PostgreSQL database.

    This is not made to store queries. Instead, another class should extend this one and add all the queries.
    That way, anyone can import this class without bringing anything useless in the process.

    Args:
        self: The reference to the object.
        user (string): The name of the user that is trying to establish a connection.
        pw (string): The password of the user.
        ip (string): The Ip adress of the database's server.
        port (string): The port number by which the connection is going to go through.
        db (string): The name of the database that is tried to be accessed.
        ssl (string): Wether SSL is required or not.
    """    
    def __init__(self,
            user = envVarsGetter.DATABASE_USER,
            pw = envVarsGetter.DATABASE_PASSWORD,
            ip = envVarsGetter.DATABASE_HOST,
            port = envVarsGetter.DATABASE_PORT,
            db = envVarsGetter.DATABASE_NAME,
            ):
        self.user = user
        self.pw = pw
        self.ip = ip
        self.port = port
        self.db = db
        self.connection_string = self.user + self.pw + self.ip + self.port + self.db

    def connectToDb(self):
        """Tries to establish a connection with the database. If fails to do so, raises an exception.

        The result of a successful connection is the connection stored in the conn attribute, and the cursor stored in the cursor attribute.

        Args:
            self: The reference to the object.
        """
        if self.connection_string != None:
            # Connect to PSQL
            try:
                self.conn = psycopg2.connect(
                    database = self.db,
                    user = self.user,
                    password = self.pw,
                    host = self.ip,
                    port = self.port,
                    )
                # Store the cursor.
                self.cursor = self.conn.cursor()
                print("Connected to the database.")
            except Exception as error:
                print ("Oops! An exception has occured:", error)
                print ("Exception TYPE:", type(error))
            return 0

    def __del__(self):
        """Terminate the connection properly when the object is about to be destroyed.

        Args:
            self: The reference to the object.
        """
        if self.conn != None:
            self.conn.close()
        if self.cursor != None:
            self.cursor.close()

