#!/usr/bin/env python3

from psycopg2 import sql
from fastapi import FastAPI, status, Request, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from src.modules import (
        eleve_mod,
        professeur_mod,
        groupe_mod,
        instrument_mod,
        competence_mod,
        domaine_mod,
        bilanelevemusiquecollective_mod,
        projet_mod,
        cycle_mod,
        elevejoincycle_mod,
        elevejoingroupe_mod,
        elevejoininstrument_mod,
        groupejoinprojet_mod,
        professeurjoingroupe_mod,
        projetjoincompetence_mod,
        monoinstrumental_mod,
        evaluationcompetencemonoinstrumental_mod 
        )

from src.dependencies.connectorConstrictorDb import dbSession
from src.config.envVars import envVarsGetter

app = FastAPI(
        title="Projet-Boa",
        description="Outil d'évaluation du Moulin à sons",
        redoc_url=None,
        swagger_ui_parameters={"docExpansion": 'none'}
        )

origins=["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.router.prefix = ""
app.include_router(router=eleve_mod.router)
app.include_router(router=professeur_mod.router)
app.include_router(router=groupe_mod.router)
app.include_router(router=instrument_mod.router)
app.include_router(router=competence_mod.router)
app.include_router(router=domaine_mod.router)
app.include_router(router=bilanelevemusiquecollective_mod.router)
app.include_router(router=projet_mod.router)
app.include_router(router=cycle_mod.router)
app.include_router(router=elevejoincycle_mod.router)
app.include_router(router=elevejoingroupe_mod.router)
app.include_router(router=elevejoininstrument_mod.router)
app.include_router(router=groupejoinprojet_mod.router)
app.include_router(router=professeurjoingroupe_mod.router)
app.include_router(router=projetjoincompetence_mod.router)
app.include_router(router=monoinstrumental_mod.router)
app.include_router(router=evaluationcompetencemonoinstrumental_mod.router)
# app.include_router(router=file_mod.router)
#app.mount("/plugins", StaticFiles(directory="src/templates/plugins"), name="plugins")
# app.mount("/dist", StaticFiles(directory="src/templates/dist"), name="dist")
app.mount("/dist", StaticFiles(directory="../vueJs/nebulosa/dist"), name="dist")
app.mount("/assets", StaticFiles(directory="../vueJs/nebulosa/dist/assets"), name="dist")

"""
@app.on_event("startup")
def startup_event():
"""

@app.get(
    "/ping",
    status_code=status.HTTP_200_OK,
    tags=["Test"],
    description="Running status",
    operation_id="Ping"
)
async def home():
    return "Success, Projet-Boa API is available!"

# Ne fonctionne pas. Mais n'est pas importante.
@app.get(
    "/test",
    tags=["Test"],
    description="Teste toutes les fonctions du backend"
    )
def test(self):
    tableDbEleve = "eleve"

    """ 
    dbSession.getListEleve(envVarsGetter.TABLE_ELEVE)
    dbSession.insertEleve(envVarsGetter.TABLE_ELEVE, "TEST", "Test", True)
    dbSession.updateEleve(envVarsGetter.TABLE_ELEVE, )
    """
    curs = self.cursor
    self.checkIfTableExist(tableDbEleve, False)

    curs.execute(sql.SQL('SELECT * FROM {}.{}').format(
        sql.Identifier(envVarsGetter.DATABASE_SCHEMA), sql.Identifier(tableDbEleve)
        )
    )

    mview = curs.fetchall()
    # print(mview)

    return "Success !"

@app.get(
        "/frontend",
        tags=["FrontEnd"],
        description="Teste le frontend"
        )
def frontEnd(
        request: Request
        ):
    dummy = ""
    templates = Jinja2Templates(directory="../vueJs/nebulosa/dist/")
    return templates.TemplateResponse("index.html", {"request" : request})
