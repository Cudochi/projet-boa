#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "bilanelevemusiquecollective" 
tableDbEleve = "eleve"
tableDbGroupe = "groupe"
tableDbCompetence = "competence"

router = APIRouter(
    prefix="/bilanelevemusiquecollectives",
    tags=["BilanEleveMusiqueCollectives"],
    ) 


@router.get(
        "/getList"
        )
def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    output = dbSession.getListBilanEleveMusiqueCollective(tableDb)

    return output

@router.get(
        "/getBilanEleveMusiqueCollectiveById"
        )
def get_BilanEleveMusiqueCollective_by_Id(
        response: Response,
        idBilanEleveMusiqueCollective: str
        ):
    output = dbSession.getBilanEleveMusiqueCollectiveById(tableDb, idBilanEleveMusiqueCollective)

    return output

@router.post(
        "/newBilanEleveMusiqueCollective"
        )
def insert_BilanEleveMusiqueCollective(
        response: Response,
        idEleve: int,
        idGroupe: int,
        idCompetence: int,
        bilanBilanEleveMusiqueCollective: str = None,
        statusCompetence: int = None,
        dateBilanEleveMusiqueCollective: str = None
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
        return HTTP_404_NOT_FOUND

    dbSession.insertBilanEleveMusiqueCollective(tableDb, 
                                                idEleve, 
                                                idGroupe, 
                                                idCompetence, 
                                                bilanBilanEleveMusiqueCollective, 
                                                statusCompetence, 
                                                dateBilanEleveMusiqueCollective
                                                )
    
    return HTTP_200_OK

@router.put(
        "/updateBilanEleveMusiqueCollective"
        )
def update_BilanEleveMusiqueCollective(
        response: Response,
        idBilanEleveMusiqueCollective: int,
        idEleve: int = None,
        idGroupe: int = None,
        idCompetence: int = None,
        bilanBilanEleveMusiqueCollective: str = None,
        statusCompetence: int = None,
        dateBilanEleveMusiqueCollective: str = None
        ):
    if idEleve != None:
        if dbSession.getEleveById(tableDbEleve, idEleve) == []:
            return HTTP_404_NOT_FOUND
    if idGroupe != None:
        if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
            return HTTP_404_NOT_FOUND
    if idCompetence != None:
        if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
            return HTTP_404_NOT_FOUND
    dbSession.updateBilanEleveMusiqueCollective(tableDb, 
                                                idBilanEleveMusiqueCollective,
                                                idEleve, 
                                                idGroupe, 
                                                idCompetence, 
                                                bilanBilanEleveMusiqueCollective, 
                                                statusCompetence, 
                                                dateBilanEleveMusiqueCollective
                                                )
 
    return HTTP_200_OK

@router.delete(
        "/deleteBilanEleveMusiqueCollective"
        )
def delete_BilanEleveMusiqueCollective(
        response: Response,
        idBilanEleveMusiqueCollective: int
        ):
    if dbSession.getBilanEleveMusiqueCollectiveById(tableDb, idBilanEleveMusiqueCollective) == []:
        return HTTP_404_NOT_FOUND
    dbSession.deleteBilanEleveMusiqueCollective(tableDb, idBilanEleveMusiqueCollective)

    return HTTP_200_OK
