#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "competence" 

router = APIRouter(
    prefix="/competences",
    tags=["Competences"],
    #dependencies=[Depends(get_token_from_header)],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    """Call the function in the dbSession object that is going to retrieve the list of students in the database.

    Returns:
        A JSON formatted text that lists the PDFs stored in the database.
    """
    output = dbSession.getListCompetence(tableDb)

    return output

@router.get(
        "/getCompetenceById"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def get_Competence_by_Id(
        response: Response,
        #user: User = Depends(get_token_from_header),
        idCompetence: str
        ):
    """Call the function in the dbSession object that is going to retrieve the list of students in the database.

    Returns:
        A JSON formatted text that lists the PDFs stored in the database.
    """
    output = dbSession.getCompetenceById(tableDb, idCompetence)

    return output

@router.post(
        "/newCompetence"
        )
def insert_Competence(
        response: Response,
        nomCompetence: str = None,
        pratiqueCompetence: str = None,
        rubriqueCompetence: str = None,
        isVisibleCompetence: bool = True
        ):

    dbSession.insertCompetence(tableDb, nomCompetence, pratiqueCompetence, rubriqueCompetence, isVisibleCompetence)
    
    return HTTP_200_OK

@router.put(
        "/updateCompetence"
        )
def update_Competence(
        response: Response,
        idCompetence: int,
        nomCompetence: str = None,
        pratiqueCompetence: str = None,
        rubriqueCompetence: str = None,
        isVisibleCompetence: bool = None
        ):
    dbSession.updateCompetence(tableDb, idCompetence, nomCompetence, pratiqueCompetence, rubriqueCompetence, isVisibleCompetence)

    return HTTP_200_OK

@router.delete(
        "/deleteCompetence"
        )
def delete_Competence(
        response: Response,
        idCompetence: int
        ):

    dbSession.deleteCompetence(tableDb, idCompetence)

    return HTTP_200_OK
