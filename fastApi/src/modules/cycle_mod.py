#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "cycle" 

router = APIRouter(
    prefix="/cycles",
    tags=["Cycles"],
    #dependencies=[Depends(get_token_from_header)],
    ) 


@router.get(
        "/getList"
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListCycle(tableDb)

    return output

@router.get(
        "/getCycleById"
        )
def get_Cycle_by_Id(
        response: Response,
        #user: User = Depends(get_token_from_header),
        idCycle: str
        ):
    output = dbSession.getCycleById(tableDb, idCycle)

    return output

@router.post(
        "/newCycle"
        )
def insert_Cycle(
        response: Response,
        numeroCycle: str = None,
        anneeCycle: str = None,
        groupeOuMonoCycle: bool = None,
        isVisibleCycle: bool = True
        ):

    dbSession.insertCycle(tableDb, numeroCycle, anneeCycle, groupeOuMonoCycle, isVisibleCycle)
    
    return HTTP_200_OK

@router.put(
        "/updateCycle"
        )
def update_Cycle(
        response: Response,
        idCycle: int,
        numeroCycle: str = None,
        anneeCycle: str = None,
        groupeOuMonoCycle: bool = None,
        isVisibleCycle: bool = True
        ):
    dbSession.updateCycle(tableDb, idCycle, numeroCycle, anneeCycle, groupeOuMonoCycle, isVisibleCycle)

    return HTTP_200_OK

@router.delete(
        "/deleteCycle"
        )
def delete_Cycle(
        response: Response,
        idCycle: int
        ):

    dbSession.deleteCycle(tableDb, idCycle)

    return HTTP_200_OK
