#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "domaine" 

router = APIRouter(
    prefix="/domaines",
    tags=["Domaines"],
    #dependencies=[Depends(get_token_from_header)],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    output = dbSession.getListDomaine(tableDb)

    return output

@router.get(
        "/getDomaineById"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def get_Domaine_by_Id(
        response: Response,
        #user: User = Depends(get_token_from_header),
        idDomaine: str
        ):
    output = dbSession.getDomaineById(tableDb, idDomaine)

    return output

@router.post(
        "/newDomaine"
        )
def insert_Domaine(
        response: Response,
        nomDomaine: str = None,
        isVisibleDomaine: bool = True
        ):

    dbSession.insertDomaine(tableDb, nomDomaine, isVisibleDomaine)
    
    return HTTP_200_OK

@router.put(
        "/updateDomaine"
        )
def update_Domaine(
        response: Response,
        idDomaine: int,
        nomDomaine: str = None,
        isVisibleDomaine: bool = None
        ):
    dbSession.updateDomaine(tableDb, idDomaine, nomDomaine, isVisibleDomaine)

    return HTTP_200_OK

@router.delete(
        "/deleteDomaine"
        )
def delete_Domaine(
        response: Response,
        idDomaine: int
        ):

    dbSession.deleteDomaine(tableDb, idDomaine)

    return HTTP_200_OK
