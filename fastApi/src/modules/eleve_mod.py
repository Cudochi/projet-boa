#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED
from src.config.envVars import envVarsGetter
tableDb = "eleve" 
tableDbEleveJoinGroupe = "eleve_join_groupe"
tableDbGroupe = "groupe"
tableDbInstrument = "instrument"
tableDbCycle = "cycle"
tableDbCompetence = "competence"
tableDbEleveJoinCycle = "eleve_join_cycle"
tableDbEleveJoinInstrument = "eleve_join_instrument"
tableDbProjetJoinCompetence = "projet_join_competence" 
tableDbGroupeJoinProjet = "groupe_join_projet" 


router = APIRouter(
    prefix="/eleves",
    tags=["Eleves"],
    #dependencies=[Depends(get_token_from_header)],
    ) 

@router.get(
        "/test",
        #description="Get the list of the students currently in the database.",
        operation_id="test",
        )
async def test(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):

    output = dbSession.test(tableDb)

    return output


@router.get(
        "/getList",
        #description="Get the list of the students currently in the database.",
        operation_id="GetList",
        )
async def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    """Call the function in the dbSession object that is going to retrieve the list of students in the database.

    Returns:
        A JSON formatted text that lists the PDFs stored in the database.
    """
    output = dbSession.getListEleve(tableDb)

    return output

@router.get(
        "/getEleveById"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
async def get_Eleve_by_Id(
        response: Response,
        #user: User = Depends(get_token_from_header),
        idEleve: str
        ):
    """Call the function in the dbSession object that is going to retrieve the list of students in the database.

    Returns:
        A JSON formatted text that lists the PDFs stored in the database.
    """
    output = dbSession.getEleveById(tableDb, idEleve)

    return output

@router.get(
        "/getElevePlusExtraById"
        )
async def get_Eleve_plus_Extra_by_Id(
        response: Response,
        idEleve: str
        ):
    """Call the function in the dbSession object that is going to retrieve the list of students in the database.

    Returns:
        A JSON formatted text that lists the PDFs stored in the database.
    """
    dataEleve           = dbSession.getEleveById(tableDb, idEleve)

    temp                = dbSession.getEleveFromEleveJoinGroupeById(tableDbEleveJoinGroupe, idEleve)
    idGroupe            = "N/A" if not temp else temp[0]['Id Groupe']

    temp_groupe          = dbSession.getGroupeById(tableDbGroupe, idGroupe)
    nomGroupe           = "N/A" if not temp_groupe else temp_groupe[0]['Nom']

    temp_eleve_cycle     = dbSession.getEleveFromEleveJoinCycleById(tableDbEleveJoinCycle, idEleve)
    idCycle             = "N/A" if not temp_eleve_cycle else temp_eleve_cycle[0]['Id Cycle']

    temp_cycle           = dbSession.getCycleById(tableDbCycle, idCycle)
    nomCycle            = "N/A" if not temp_cycle else temp_cycle[0]['Numero']
    anneeCycle          = "N/A" if not temp_cycle else temp_cycle[0]['Annee']

    temp_eleve_instrument = dbSession.getEleveFromEleveJoinInstrumentById(tableDbEleveJoinInstrument, idEleve)
    idInstrument        = "N/A" if not temp_eleve_instrument else temp_eleve_instrument[0]['Id Instrument']

    temp_instrument      = dbSession.getInstrumentById(tableDbInstrument, idInstrument)
    nomInstrument       = "N/A" if not temp_instrument else temp_instrument[0]['Nom']

    temp_groupe_projet   = dbSession.getGroupeFromGroupeJoinProjetById(tableDbGroupeJoinProjet, idGroupe)
    idProjet            = "N/A" if not temp_groupe_projet else temp_groupe_projet[0]['Id Projet']

    listeIdCompetences  = dbSession.getCompetenceListFromProjetJoinCompetenceById(tableDbProjetJoinCompetence, idProjet)

    listeCompetences = []
    for competence in listeIdCompetences:
        listeCompetences.append(dbSession.getCompetenceById(tableDbCompetence, competence["Id Competence"]))


    dataEleve[0]['Groupe']      = nomGroupe
    dataEleve[0]['Nom Cycle']   = nomCycle
    dataEleve[0]['Annee Cycle'] = anneeCycle
    dataEleve[0]['Instrument']  = nomInstrument
    dataEleve[0]['Competences'] = listeCompetences 

    print(dataEleve)

    return dataEleve

@router.post(
        "/newEleve"
        )
async def insert_Eleve(
        response: Response,
        nomEleve: str = None,
        prenomEleve: str = None,
        isVisibleEleve: bool = True
        ):

    dbSession.insertEleve(tableDb, nomEleve, prenomEleve, isVisibleEleve)
    
    return HTTP_200_OK

@router.post(
        "/newElevePlusExtra"
        )
async def insert_Eleve_Plus_Extra(
        response: Response,
        nomEleve: str = "",
        prenomEleve: str = "",
        isVisibleEleve: bool = True,
        idGoupe: int = 1,
        idInstrument: int = 1,
        idCycle: int = 1,
        ):

    idEleve  = dbSession.insertEleve(tableDb, nomEleve, prenomEleve, isVisibleEleve)

    dbSession.insertEleveJoinGroupe(tableDbEleveJoinGroupe, idEleve, idGoupe)
    dbSession.insertEleveJoinInstrument(tableDbEleveJoinInstrument, idEleve, idInstrument)
    dbSession.insertEleveJoinCycle(tableDbEleveJoinCycle, idEleve, idCycle)
    
    return HTTP_200_OK

@router.put(
        "/updateEleve"
        )
async def update_Eleve(
        response: Response,
        idEleve: int,
        nomEleve: str = "",
        prenomEleve: str = "",
        isVisibleEleve: bool = True
        ):
    dbSession.updateEleve(tableDb, idEleve, nomEleve, prenomEleve, isVisibleEleve)

    return HTTP_200_OK

@router.delete(
        "/deleteEleve"
        )
async def delete_Eleve(
        response: Response,
        idEleve: int
        ):

    dbSession.deleteEleve(tableDb, idEleve)

    return HTTP_200_OK
