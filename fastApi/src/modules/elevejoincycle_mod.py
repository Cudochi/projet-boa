#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED, HTTP_409_CONFLICT, HTTP_410_GONE

tableDb = "eleve_join_cycle" 
tableDbEleve = "eleve" 
tableDbCycle = "cycle" 

router = APIRouter(
    prefix="/eleve_join_cycles",
    tags=["EleveJoinCycles"],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListEleveJoinCycle(tableDb)

    return output

@router.get(
        "/getEleveFromEleveJoinCycleById"
        )
def get_Eleve_From_EleveJoinCycle_by_Id(
        response: Response,
        idEleve: str
        ):
    output = dbSession.getEleveFromEleveJoinCycleById(tableDb, idEleve)

    return output

@router.get(
        "/getCycleFromEleveJoinCycleById"
        )
def get_Cycle_From_EleveJoinCycle_by_Id(
        response: Response,
        idCycle: str
        ):
    output = dbSession.getCycleFromEleveJoinCycleById(tableDb, idCycle)

    return output

@router.post(
        "/newEleveJoinCycle"
        )
def insert_EleveJoinCycle(
        response: Response,
        idEleve: str,
        idCycle: str,
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getCycleById(tableDbCycle, idCycle) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEleveJoinCycleById(tableDb, idEleve, idCycle) != []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.insertEleveJoinCycle(tableDb, idEleve, idCycle)
    
    return HTTP_200_OK

@router.put(
        "/updateEleveJoinCycle"
        )
def update_EleveJoinCycle(
        response: Response,
        idEleve: int,
        idCycle: int = None,
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if idCycle != None:
        if dbSession.getCycleById(tableDbCycle, idCycle) == []:
            return HTTP_404_NOT_FOUND
        if dbSession.getPairFromEleveJoinCycleById(tableDb, idEleve, idCycle) != []:
            return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.updateEleveJoinCycle(tableDb, idEleve, idCycle)

    return HTTP_200_OK

@router.delete(
        "/deleteEleveJoinCycle"
        )
def delete_EleveJoinCycle(
        response: Response,
        idEleve: int,
        idCycle: int
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getCycleById(tableDbCycle, idCycle) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEleveJoinCycleById(tableDb, idEleve, idCycle) == []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.deletePairFromEleveJoinCycle(tableDb, idEleve, idCycle)

    return HTTP_200_OK
