#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED, HTTP_409_CONFLICT, HTTP_410_GONE

tableDb = "eleve_join_groupe" 
tableDbEleve = "eleve" 
tableDbGroupe = "groupe" 

router = APIRouter(
    prefix="/eleve_join_groupes",
    tags=["EleveJoinGroupes"],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListEleveJoinGroupe(tableDb)

    return output

@router.get(
        "/getEleveFromEleveJoinGroupeById"
        )
def get_Eleve_From_EleveJoinGroupe_by_Id(
        response: Response,
        idEleve: str
        ):
    output = dbSession.getEleveFromEleveJoinGroupeById(tableDb, idEleve)

    return output

@router.get(
        "/getGroupeFromEleveJoinGroupeById"
        )
def get_Groupe_From_EleveJoinGroupe_by_Id(
        response: Response,
        idGroupe: str
        ):
    output = dbSession.getGroupeFromEleveJoinGroupeById(tableDb, idGroupe)

    return output

@router.post(
        "/newEleveJoinGroupe"
        )
def insert_EleveJoinGroupe(
        response: Response,
        idEleve: str,
        idGroupe: str,
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEleveJoinGroupeById(tableDb, idEleve, idGroupe) != []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.insertEleveJoinGroupe(tableDb, idEleve, idGroupe)
    
    return HTTP_200_OK

@router.put(
        "/updateEleveJoinGroupe"
        )
def update_EleveJoinGroupe(
        response: Response,
        idEleve: int,
        idGroupe: int = None,
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if idGroupe != None:
        if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
            return HTTP_404_NOT_FOUND
        if dbSession.getPairFromEleveJoinGroupeById(tableDb, idEleve, idGroupe) != []:
            return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.updateEleveJoinGroupe(tableDb, idEleve, idGroupe)

    return HTTP_200_OK

@router.delete(
        "/deleteEleveJoinGroupe"
        )
def delete_EleveJoinGroupe(
        response: Response,
        idEleve: int,
        idGroupe: int
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEleveJoinGroupeById(tableDb, idEleve, idGroupe) == []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.deletePairFromEleveJoinGroupe(tableDb, idEleve, idGroupe)

    return HTTP_200_OK
