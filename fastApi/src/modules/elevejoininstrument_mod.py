#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED, HTTP_409_CONFLICT, HTTP_410_GONE

tableDb = "eleve_join_instrument" 
tableDbEleve = "eleve" 
tableDbInstrument = "instrument" 

router = APIRouter(
    prefix="/eleve_join_instruments",
    tags=["EleveJoinInstruments"],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListEleveJoinInstrument(tableDb)

    return output

@router.get(
        "/getEleveFromEleveJoinInstrumentById"
        )
def get_Eleve_From_EleveJoinInstrument_by_Id(
        response: Response,
        idEleve: str
        ):
    output = dbSession.getEleveFromEleveJoinInstrumentById(tableDb, idEleve)

    return output

@router.get(
        "/getInstrumentFromEleveJoinInstrumentById"
        )
def get_Instrument_From_EleveJoinInstrument_by_Id(
        response: Response,
        idInstrument: str
        ):
    output = dbSession.getInstrumentFromEleveJoinInstrumentById(tableDb, idInstrument)

    return output

@router.post(
        "/newEleveJoinInstrument"
        )
def insert_EleveJoinInstrument(
        response: Response,
        idEleve: str,
        idInstrument: str,
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getInstrumentById(tableDbInstrument, idInstrument) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEleveJoinInstrumentById(tableDb, idEleve, idInstrument) != []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.insertEleveJoinInstrument(tableDb, idEleve, idInstrument)
    
    return HTTP_200_OK

@router.put(
        "/updateEleveJoinInstrument"
        )
def update_EleveJoinInstrument(
        response: Response,
        idEleve: int,
        idInstrument: int = None,
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if idInstrument != None:
        if dbSession.getInstrumentById(tableDbInstrument, idInstrument) == []:
            return HTTP_404_NOT_FOUND
        if dbSession.getPairFromEleveJoinInstrumentById(tableDb, idEleve, idInstrument) != []:
            return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.updateEleveJoinInstrument(tableDb, idEleve, idInstrument)

    return HTTP_200_OK

@router.delete(
        "/deleteEleveJoinInstrument"
        )
def delete_EleveJoinInstrument(
        response: Response,
        idEleve: int,
        idInstrument: int
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getInstrumentById(tableDbInstrument, idInstrument) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEleveJoinInstrumentById(tableDb, idEleve, idInstrument) == []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.deletePairFromEleveJoinInstrument(tableDb, idEleve, idInstrument)

    return HTTP_200_OK
