#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED, HTTP_409_CONFLICT, HTTP_410_GONE

tableDb = "evaluationcompetencemonoinstrumental" 
tableDbMonoinstrumental = "monoinstrumental" 
tableDbCompetence = "competence" 

router = APIRouter(
    prefix="/evaluationcompetencemonoinstrumentales",
    tags=["EvaluationCompetenceMonoinstrumentals"],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListEvaluationCompetenceMonoinstrumental(tableDb)

    return output

@router.get(
        "/getCompetenceFromEvaluationCompetenceMonoinstrumentalById"
        )
def get_Evaluation_From_EvaluationCompetenceMonoinstrumental_by_Id(
        response: Response,
        idMonoinstrumental: str
        ):
    output = dbSession.getEvaluationFromEvaluationCompetenceMonoinstrumentalById(tableDb, idMonoinstrumental)

    return output

@router.get(
        "/getProjetFromEvaluationCompetenceMonoinstrumentalById"
        )
def get_Pair_From_EvaluationCompetenceMonoinstrumental_by_Id(
        response: Response,
        idMonoinstrumental: int,
        idCompetence: int
        ):
    output = dbSession.getPairFromEvaluationCompetenceMonoinstrumentalById(tableDb, idMonoinstrumental, idCompetence)

    return output

@router.post(
        "/newEvaluationCompetenceMonoinstrumental"
        )
def insert_EvaluationCompetenceMonoinstrumental(
        response: Response,
        idMonoinstrumental: int,
        idCompetence: int,
        statusCompetence: int = None,
        ):
    if dbSession.getMonoinstrumentalById(tableDbMonoinstrumental, idMonoinstrumental) == []:
        print("test0")
        return HTTP_404_NOT_FOUND
    if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
        print("test1")
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEvaluationCompetenceMonoinstrumentalById(tableDb, idMonoinstrumental, idCompetence) != []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.insertEvaluationCompetenceMonoinstrumental(tableDb,idMonoinstrumental, idCompetence, statusCompetence)
    
    return HTTP_200_OK

@router.put(
        "/updateEvaluationCompetenceMonoinstrumental"
        )
def update_EvaluationCompetenceMonoinstrumental(
        response: Response,
        idMonoinstrumental: int,
        idCompetence: int,
        statusCompetence: int = None,
        ):
    if dbSession.getMonoinstrumentalById(tableDbMonoinstrumental, idMonoinstrumental) == []:
        return HTTP_404_NOT_FOUND
    if idCompetence != None:
        if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
            return HTTP_404_NOT_FOUND

    dbSession.updateEvaluationCompetenceMonoinstrumental(tableDb, idMonoinstrumental, idCompetence, statusCompetence)

    return HTTP_200_OK

@router.delete(
        "/deleteEvaluationCompetenceMonoinstrumental"
        )
def delete_EvaluationCompetenceMonoinstrumental(
        response: Response,
        idMonoinstrumental: int,
        idCompetence: int,
        ):
    if dbSession.getMonoinstrumentalById(tableDbMonoinstrumental, idMonoinstrumental) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromEvaluationCompetenceMonoinstrumentalById(tableDb, idMonoinstrumental, idCompetence) == []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.deletePairFromEvaluationCompetenceMonoinstrumental(tableDb, idMonoinstrumental, idCompetence)

    return HTTP_200_OK
