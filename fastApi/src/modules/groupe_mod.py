#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "groupe" 
tableDbEleveJoinGroupe = "eleve_join_groupe"

router = APIRouter(
    prefix="/groupes",
    tags=["Groupes"],
    #dependencies=[Depends(get_token_from_header)],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
async def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    output = dbSession.getListGroupe(tableDb)

    return output

@router.get(
        "/getGroupeById"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
async def get_Groupe_by_Id(
        response: Response,
        #user: User = Depends(get_token_from_header),
        idGroupe: str
        ):
    output = dbSession.getGroupeById(tableDb, idGroupe)

    return output

@router.get(
        "/getGroupePlusEleveById"
        )
async def get_Groupe_plus_Eleve_by_Id(
        response: Response,
        idGroupe: str
        ):
    dataGroupe = dbSession.getGroupeById(tableDb, idGroupe)
    listGroupe = dbSession.getGroupeFromEleveJoinGroupeById(tableDbEleveJoinGroupe, idGroupe)
    listeEleve = []
    for item in listGroupe:
            idEleve = item["Id Eleve"]
            dataEleve = dbSession.getEleveById("eleve", idEleve)
            idCycle = dbSession.getEleveFromEleveJoinCycleById("eleve_join_cycle", idEleve)[0]['Id Cycle']
            nomCycle = dbSession.getCycleById("cycle", idCycle)[0]['Numero']
            idInstrument = dbSession.getEleveFromEleveJoinInstrumentById("eleve_join_instrument", idEleve)[0]['Id Instrument']
            nomInstrument = dbSession.getInstrumentById("instrument", idInstrument)[0]['Nom']
            dataEleve[0]['Nom Cycle'] = nomCycle
            dataEleve[0]['Instrument']  = nomInstrument
            listeEleve.append(dataEleve)
            print("balise 1")
            print(listeEleve)

    print("balise 3")
    dataGroupe = listeEleve
    print(dataGroupe)

    return dataGroupe

@router.post(
        "/newGroupe"
        )
async def insert_Groupe(
        response: Response,
        nomGroupe: str = None,
        collectifOuAtelierGroupe: bool = None,
        isVisibleGroupe: bool = True
        ):

    dbSession.insertGroupe(tableDb, nomGroupe, collectifOuAtelierGroupe, isVisibleGroupe)
    
    return HTTP_200_OK

@router.put(
        "/updateGroupe"
        )
async def update_Groupe(
        response: Response,
        idGroupe: int,
        nomGroupe: str = None,
        collectifOuAtelierGroupe: bool = None,
        isVisibleGroupe: bool = None
        ):
    dbSession.updateGroupe(tableDb, idGroupe, nomGroupe, collectifOuAtelierGroupe, isVisibleGroupe)

    return HTTP_200_OK

@router.delete(
        "/deleteGroupe"
        )
async def delete_Groupe(
        response: Response,
        idGroupe: int
        ):

    dbSession.deleteGroupe(tableDb, idGroupe)

    return HTTP_200_OK
