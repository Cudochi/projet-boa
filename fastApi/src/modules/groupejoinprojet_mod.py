#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED, HTTP_409_CONFLICT, HTTP_410_GONE

tableDb = "groupe_join_projet" 
tableDbGroupe = "groupe" 
tableDbProjet = "projet" 

router = APIRouter(
    prefix="/groupe_join_projets",
    tags=["GroupeJoinProjets"],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListGroupeJoinProjet(tableDb)

    return output

@router.get(
        "/getGroupeFromGroupeJoinProjetById"
        )
def get_Groupe_From_GroupeJoinProjet_by_Id(
        response: Response,
        idGroupe: str
        ):
    output = dbSession.getGroupeFromGroupeJoinProjetById(tableDb, idGroupe)

    return output

@router.get(
        "/getProjetFromGroupeJoinProjetById"
        )
def get_Projet_From_GroupeJoinProjet_by_Id(
        response: Response,
        idProjet: str
        ):
    output = dbSession.getProjetFromGroupeJoinProjetById(tableDb, idProjet)

    return output

@router.post(
        "/newGroupeJoinProjet"
        )
def insert_GroupeJoinProjet(
        response: Response,
        idGroupe: str,
        idProjet: str,
        ):
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getProjetById(tableDbProjet, idProjet) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromGroupeJoinProjetById(tableDb, idGroupe, idProjet) != []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.insertGroupeJoinProjet(tableDb, idGroupe, idProjet)
    
    return HTTP_200_OK

@router.put(
        "/updateGroupeJoinProjet"
        )
def update_GroupeJoinProjet(
        response: Response,
        idGroupe: int,
        idProjet: int = None,
        ):
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if idProjet != None:
        if dbSession.getProjetById(tableDbProjet, idProjet) == []:
            return HTTP_404_NOT_FOUND
        if dbSession.getPairFromGroupeJoinProjetById(tableDb, idGroupe, idProjet) != []:
            return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.updateGroupeJoinProjet(tableDb, idGroupe, idProjet)

    return HTTP_200_OK

@router.delete(
        "/deleteGroupeJoinProjet"
        )
def delete_GroupeJoinProjet(
        response: Response,
        idGroupe: int,
        idProjet: int
        ):
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getProjetById(tableDbProjet, idProjet) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromGroupeJoinProjetById(tableDb, idGroupe, idProjet) == []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.deletePairFromGroupeJoinProjet(tableDb, idGroupe, idProjet)

    return HTTP_200_OK
