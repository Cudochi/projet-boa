#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "instrument" 

router = APIRouter(
    prefix="/instruments",
    tags=["Instruments"],
    #dependencies=[Depends(get_token_from_header)],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    output = dbSession.getListInstrument(tableDb)

    return output

@router.get(
        "/getInstrumentById"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def get_Instrument_by_Id(
        response: Response,
        #user: User = Depends(get_token_from_header),
        idInstrument: str
        ):
    output = dbSession.getInstrumentById(tableDb, idInstrument)

    return output

@router.post(
        "/newInstrument"
        )
def insert_Instrument(
        response: Response,
        nomInstrument: str = None,
        isVisibleInstrument: bool = True
        ):

    dbSession.insertInstrument(tableDb, nomInstrument, isVisibleInstrument)
    
    return HTTP_200_OK

@router.put(
        "/updateInstrument"
        )
def update_Instrument(
        response: Response,
        idInstrument: int,
        nomInstrument: str = None,
        isVisibleInstrument: bool = None
        ):
    dbSession.updateInstrument(tableDb, idInstrument, nomInstrument, isVisibleInstrument)

    return HTTP_200_OK

@router.delete(
        "/deleteInstrument"
        )
def delete_Instrument(
        response: Response,
        idInstrument: int
        ):

    dbSession.deleteInstrument(tableDb, idInstrument)

    return HTTP_200_OK
