#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "monoinstrumental" 
tableDbEleve = "eleve"
tableDbProfesseur = "professeur"

router = APIRouter(
    prefix="/monoinstrumentals",
    tags=["Monoinstrumentals"],
    ) 


@router.get(
        "/getList"
        )
def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    output = dbSession.getListMonoinstrumental(tableDb)

    return output

@router.get(
        "/getMonoinstrumentalById"
        )
def get_Monoinstrumental_by_Id(
        response: Response,
        idMonoinstrumental: str
        ):
    output = dbSession.getMonoinstrumentalById(tableDb, idMonoinstrumental)

    return output

@router.post(
        "/newMonoinstrumental"
        )
def insert_Monoinstrumental(
        response: Response,
        idEleve: int,
        idProfesseur: int,
        bilanMonoinstrumental: str = None,
        dateMonoinstrumental: str = None
        ):
    if dbSession.getEleveById(tableDbEleve, idEleve) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getProfesseurById(tableDbProfesseur, idProfesseur) == []:
        return HTTP_404_NOT_FOUND

    dbSession.insertMonoinstrumental(tableDb, 
                                                idEleve, 
                                                idProfesseur, 
                                                bilanMonoinstrumental, 
                                                dateMonoinstrumental
                                                )
    
    return HTTP_200_OK

@router.put(
        "/updateMonoinstrumental"
        )
def update_Monoinstrumental(
        response: Response,
        idMonoinstrumental: int,
        idEleve: int = None,
        idProfesseur: int = None,
        bilanMonoinstrumental: str = None,
        dateMonoinstrumental: str = None
        ):
    if idEleve != None:
        if dbSession.getEleveById(tableDbEleve, idEleve) == []:
            return HTTP_404_NOT_FOUND
    if idProfesseur != None:
        if dbSession.getProfesseurById(tableDbProfesseur, idProfesseur) == []:
            return HTTP_404_NOT_FOUND
    dbSession.updateMonoinstrumental(tableDb, 
                                    idMonoinstrumental,
                                    idEleve, 
                                    idProfesseur, 
                                    bilanMonoinstrumental, 
                                    dateMonoinstrumental
                                    )
 
    return HTTP_200_OK

@router.delete(
        "/deleteMonoinstrumental"
        )
def delete_Monoinstrumental(
        response: Response,
        idMonoinstrumental: int
        ):
    if dbSession.getMonoinstrumentalById(tableDb, idMonoinstrumental) == []:
        return HTTP_404_NOT_FOUND
    dbSession.deleteMonoinstrumental(tableDb, idMonoinstrumental)

    return HTTP_200_OK
