#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "professeur"

router = APIRouter(
    prefix="/professeurs",
    tags=["Professeurs"],
    #dependencies=[Depends(get_token_from_header)],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the professors currently in the database.",
        #operation_id="GetList",
        )
async def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    output = dbSession.getListProfesseur(tableDb)

    return output

@router.get(
        "/getProfesseurById"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
async def get_Professeur_by_Id(
        response: Response,
        #user: User = Depends(get_token_from_header),
        idProfesseur: str
        ):
    output = dbSession.getProfesseurById(tableDb, idProfesseur)

    return output

@router.post(
        "/newProfesseur"
        )
async def insert_Professeur(
        response: Response,
        nomProfesseur: str = None,
        prenomProfesseur: str = None,
        isVisibleProfesseur: bool = True
        ):

    dbSession.insertProfesseur(tableDb, nomProfesseur, prenomProfesseur, isVisibleProfesseur)
    
    return HTTP_200_OK

@router.put(
        "/updateProfesseur"
        )
async def update_Professeur(
        response: Response,
        idProfesseur: int,
        nomProfesseur: str = None,
        prenomProfesseur: str = None,
        isVisibleProfesseur: bool = None
        ):
    dbSession.updateProfesseur(tableDb, idProfesseur, nomProfesseur, prenomProfesseur, isVisibleProfesseur)

    return HTTP_200_OK

@router.delete(
        "/deleteProfesseur"
        )
async def delete_Professeur(
        response: Response,
        idProfesseur: int
        ):

    dbSession.deleteProfesseur(tableDb, idProfesseur)

    return HTTP_200_OK
