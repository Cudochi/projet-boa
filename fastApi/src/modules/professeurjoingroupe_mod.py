#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED, HTTP_409_CONFLICT, HTTP_410_GONE

tableDb = "professeur_join_groupe" 
tableDbProfesseur = "professeur" 
tableDbGroupe = "groupe" 

router = APIRouter(
    prefix="/professeur_join_groupes",
    tags=["ProfesseurJoinGroupes"],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListProfesseurJoinGroupe(tableDb)

    return output

@router.get(
        "/getProfesseurFromProfesseurJoinGroupeById"
        )
def get_Professeur_From_ProfesseurJoinGroupe_by_Id(
        response: Response,
        idProfesseur: str
        ):
    output = dbSession.getProfesseurFromProfesseurJoinGroupeById(tableDb, idProfesseur)

    return output

@router.get(
        "/getGroupeFromProfesseurJoinGroupeById"
        )
def get_Groupe_From_ProfesseurJoinGroupe_by_Id(
        response: Response,
        idGroupe: str
        ):
    output = dbSession.getGroupeFromProfesseurJoinGroupeById(tableDb, idGroupe)

    return output

@router.post(
        "/newProfesseurJoinGroupe"
        )
def insert_ProfesseurJoinGroupe(
        response: Response,
        idProfesseur: str,
        idGroupe: str,
        ):
    if dbSession.getProfesseurById(tableDbProfesseur, idProfesseur) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromProfesseurJoinGroupeById(tableDb, idProfesseur, idGroupe) != []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.insertProfesseurJoinGroupe(tableDb, idProfesseur, idGroupe)
    
    return HTTP_200_OK

@router.put(
        "/updateProfesseurJoinGroupe"
        )
def update_ProfesseurJoinGroupe(
        response: Response,
        idProfesseur: int,
        idGroupe: int = None,
        ):
    if dbSession.getProfesseurById(tableDbProfesseur, idProfesseur) == []:
        return HTTP_404_NOT_FOUND
    if idGroupe != None:
        if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
            return HTTP_404_NOT_FOUND
        if dbSession.getPairFromProfesseurJoinGroupeById(tableDb, idProfesseur, idGroupe) != []:
            return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.updateProfesseurJoinGroupe(tableDb, idProfesseur, idGroupe)

    return HTTP_200_OK

@router.delete(
        "/deleteProfesseurJoinGroupe"
        )
def delete_ProfesseurJoinGroupe(
        response: Response,
        idProfesseur: int,
        idGroupe: int
        ):
    if dbSession.getProfesseurById(tableDbProfesseur, idProfesseur) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getGroupeById(tableDbGroupe, idGroupe) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromProfesseurJoinGroupeById(tableDb, idProfesseur, idGroupe) == []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.deletePairFromProfesseurJoinGroupe(tableDb, idProfesseur, idGroupe)

    return HTTP_200_OK
