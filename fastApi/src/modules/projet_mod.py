#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED

tableDb = "projet" 

router = APIRouter(
    prefix="/projets",
    tags=["Projets"],
    ) 


@router.get(
        "/getList"
        )
async def list_Data(
        response: Response,
        #user: User = Depends(get_token_from_header),
        ):
    output = dbSession.getListProjet(tableDb)

    return output

@router.get(
        "/getProjetById"
        )
async def get_Projet_by_Id(
        response: Response,
        idProjet: str
        ):
    output = dbSession.getProjetById(tableDb, idProjet)

    return output

@router.post(
        "/newProjet"
        )
async def insert_Projet(
        response: Response,
        nomProjet: str = None,
        dateDebutProjet: str = None,
        dateFinProjet: str = None,
        presentationProjet: str = None,
        bilanProjet: str = None,
        visibleProjet: bool = True 
        ):
    dbSession.insertProjet(tableDb, 
                            nomProjet, 
                            dateDebutProjet, 
                            dateFinProjet, 
                            presentationProjet, 
                            bilanProjet, 
                            visibleProjet
                            )
    
    return HTTP_200_OK

@router.put(
        "/updateProjet"
        )
async def update_Projet(
        response: Response,
        idProjet: int,
        nomProjet: str = None,
        dateDebutProjet: str = None,
        dateFinProjet: str = None,
        presentationProjet: str = None,
        bilanProjet: str = None,
        visibleProjet: bool = True
        ):
    dbSession.updateProjet(tableDb, 
                            idProjet,
                            nomProjet, 
                            dateDebutProjet, 
                            dateFinProjet, 
                            presentationProjet, 
                            bilanProjet, 
                            visibleProjet
                            )

    return HTTP_200_OK

@router.delete(
        "/deleteProjet"
        )
async def delete_Projet(
        response: Response,
        idProjet: int
        ):

    dbSession.deleteProjet(tableDb, idProjet)

    return HTTP_200_OK
