#!/usr/bin/env python3

import os
import hashlib
from fastapi import APIRouter, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import FileResponse, Response
from starlette.background import BackgroundTasks
from src.dependencies.connectorConstrictorDb import dbSession
# from src.dependencies.utils.common import remove_file, generateUuid
# from src.dependencies.auth_session import get_token_from_header, User
# from src.dependencies.azure.blobStorage.azureServiceClient import azureSession
from starlette.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_405_METHOD_NOT_ALLOWED, HTTP_409_CONFLICT, HTTP_410_GONE

tableDb = "projet_join_competence" 
tableDbProjet = "projet" 
tableDbCompetence = "competence" 

router = APIRouter(
    prefix="/projet_join_competences",
    tags=["ProjetJoinCompetences"],
    ) 


@router.get(
        "/getList"
        #description="Get the list of the students currently in the database.",
        #operation_id="GetList",
        )
def list_Data(
        response: Response,
        ):
    output = dbSession.getListProjetJoinCompetence(tableDb)

    return output

@router.get(
        "/getCompetenceListFromProjetJoinCompetenceById"
        )
def get_Competence_List_From_ProjetJoinCompetence_by_Id(
        response: Response,
        idProjet: str
        ):
    output = dbSession.getCompetenceListFromProjetJoinCompetenceById(tableDb, idProjet)

    return output

@router.get(
        "/getProjetFromProjetJoinCompetenceById"
        )
def get_Projet_From_ProjetJoinCompetence_by_Id(
        response: Response,
        idProjet: str
        ):
    output = dbSession.getProjetFromProjetJoinCompetenceById(tableDb, idProjet)

    return output

@router.get(
        "/getCompetenceFromProjetJoinCompetenceById"
        )
def get_Competence_From_ProjetJoinCompetence_by_Id(
        response: Response,
        idCompetence: str
        ):
    output = dbSession.getCompetenceFromProjetJoinCompetenceById(tableDb, idCompetence)

    return output

@router.post(
        "/newProjetJoinCompetence"
        )
def insert_ProjetJoinCompetence(
        response: Response,
        idProjet: str,
        idCompetence: str,
        ):
    if dbSession.getProjetById(tableDbProjet, idProjet) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromProjetJoinCompetenceById(tableDb, idProjet, idCompetence) != []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.insertProjetJoinCompetence(tableDb, idProjet, idCompetence)
    
    return HTTP_200_OK

@router.put(
        "/updateProjetJoinCompetence"
        )
def update_ProjetJoinCompetence(
        response: Response,
        idProjet: int,
        idCompetence: int = None,
        ):
    if dbSession.getProjetById(tableDbProjet, idProjet) == []:
        return HTTP_404_NOT_FOUND
    if idCompetence != None:
        if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
            return HTTP_404_NOT_FOUND
        if dbSession.getPairFromProjetJoinCompetenceById(tableDb, idProjet, idCompetence) != []:
            return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.updateProjetJoinCompetence(tableDb, idProjet, idCompetence)

    return HTTP_200_OK

@router.delete(
        "/deleteProjetJoinCompetence"
        )
def delete_ProjetJoinCompetence(
        response: Response,
        idProjet: int,
        idCompetence: int
        ):
    if dbSession.getProjetById(tableDbProjet, idProjet) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getCompetenceById(tableDbCompetence, idCompetence) == []:
        return HTTP_404_NOT_FOUND
    if dbSession.getPairFromProjetJoinCompetenceById(tableDb, idProjet, idCompetence) == []:
        return HTTP_405_METHOD_NOT_ALLOWED

    dbSession.deletePairFromProjetJoinCompetence(tableDb, idProjet, idCompetence)

    return HTTP_200_OK
