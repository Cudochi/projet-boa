import psycopg2

# Database connection details
host = "127.0.0.1"
port = "5433"
user = "kavan"
password = "kavan"
database = "constrictor"

# Establish a connection to the PostgreSQL database
conn = psycopg2.connect(
    host=host,
    port=port,
    user=user,
    password=password,
    database=database
)

# Create a cursor object to execute SQL queries
cursor = conn.cursor()

# Execute the query to list tables from all databases
cursor.execute("SET search_path TO constrictor")
cursor.execute(
    "SELECT * FROM eleve"
    )

# print(cursor.fetchall())


# Close the cursor and the database connection
cursor.close()
conn.close()
