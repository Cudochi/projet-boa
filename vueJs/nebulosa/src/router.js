import { createRouter, createWebHistory } from 'vue-router'
import Recherche from './components/Recherche.vue'
import EleveList  from './components/EleveList.vue'
import BilanEleve from './components/BilanEleve.vue'
import ProfList from './components/ProfList.vue'
import GroupeList from './components/GroupeList.vue'
import BilanGroupe from './components/BilanGroupe.vue'
import ProjetList from './components/ProjetList.vue'
import BilanProjet from './components/BilanProjet.vue'

const routes = [
  { path: '/recherche',   component: Recherche    },
  { path: '/elevelist',   component: EleveList    },
  { path: '/bilaneleve',  component: BilanEleve   },
  { path: '/proflist',    component: ProfList     },
  { path: '/groupelist',  component: GroupeList   },
  { path: '/bilangroupe', component: BilanGroupe  },
  { path: '/projetlist',  component: ProjetList   },
  { path: '/bilanprojet', component: BilanProjet  },
  // Add more routes as needed
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
