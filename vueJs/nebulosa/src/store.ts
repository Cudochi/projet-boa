import { createStore } from 'vuex';
import axios from 'axios';

export default createStore({
  state: {
    detailsData: {}
  },
  mutations: {
    setDetailsData(state, data) {
      // state.detailsData = data;
      state.detailsData = { ...data };
    }
  },
  actions: {
    async fetchDetailsData({ commit }, itemArray) {
      try {
        // Ensure that the item array contains at least one item
        if (itemArray.length === 0) {
          throw new Error("Item array is empty.");
        }

        // Extract the Id from the first item in the array
        const itemId = itemArray[0].Id;
        console.log('Fetching data for item ID:', itemId);

        const url = `/api/eleves/getElevePlusExtraById?idEleve=${itemId}`;
        console.log('Request URL Store :', url);  // Log the full URL for debugging

        const response = await axios.get(url);
        commit('setDetailsData', response.data);
        console.log('Committed Data:', response.data);
      } catch (error) {
        console.error(error);
      }
    }
  }
});
